# Holeg Simulator

Cyber-physical systems simulation software following a Holon-based smart grid system.

## Prerequisites

* Java 1.8 compatible VM

## Introduction and User Manual

The Wiki contains some general [Information](https://git.tk.informatik.tu-darmstadt.de/carlos.garcia/praktikum-holons/wiki/Introduction) and a [User Manual](https://git.tk.informatik.tu-darmstadt.de/carlos.garcia/praktikum-holons/wiki/User+Manual)

## API Instructions

Check out the [API Instructions Wiki](https://git.tk.informatik.tu-darmstadt.de/carlos.garcia/praktikum-holons/wiki/Algorithms) for some Instructions

## License

This project is licensed under a modified MIT License - see the [LICENSE.md](https://git.tk.informatik.tu-darmstadt.de/carlos.garcia/praktikum-holons/src/master/license.md) file for details
	
## Built With

* [JUnit](http://junit.org) - The used test system
* [Gson](https://github.com/google/gson) - Used for Saving/Loading
* [Apache Common Compress ](https://rometools.github.io/rome/) - Creates zip/tar Archives

## Contributors

### Version 1.0 to 1.1.0

*  Kevin Julian Trometer
*  Dominik Fabio Rieder
*  Teh-Hai Julian Zheng
*  Edgardo Ernesto Palza Paredes
*  Jessey Steven Widhalm

### Version 2.0

*  Isabella Dix
*  Carlos Garcia

### Version 2.1 [Changelog](https://git.tk.informatik.tu-darmstadt.de/carlos.garcia/praktikum-holons/src/master/ChangelogV2_1.md)

*  Andreas T. Meyer-Berg
*  Ludwig Tietze
*  Antonio Schneider
*  Tom Troppmann

## Under contract from:

*  Darmstadt University of Technology
*  Department of Computer Science, Telecooperation
*  Hochschulstr. 10
*  Informatik Telecooperation Lab (TK)
*  D-64289 Darmstadt, Germany

### Version 1.0 to 1.1.0

*  Andreas Tundis
*  Carlos Garcia

### Version 2.1

*  Rolf Egert
