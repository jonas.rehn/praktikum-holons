package TypeAdapter;

import java.awt.Color;
import java.lang.reflect.Type;

import com.google.gson.JsonDeserializationContext;
import com.google.gson.JsonDeserializer;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParseException;
import com.google.gson.JsonPrimitive;
import com.google.gson.JsonSerializationContext;
import com.google.gson.JsonSerializer;

public class ColorAdapter implements JsonSerializer<Color>, JsonDeserializer<Color> {

	@Override
	public Color deserialize(JsonElement arg0, Type arg1, JsonDeserializationContext arg2) throws JsonParseException {
		JsonObject object = arg0.getAsJsonObject();
		return new Color(object.get("R").getAsInt(), object.get("G").getAsInt(), object.get("B").getAsInt());
		
	}

	@Override
	public JsonElement serialize(Color arg0, Type arg1, JsonSerializationContext arg2) {
		JsonObject object = new JsonObject();
		object.add("R", new JsonPrimitive(arg0.getRed()));
		object.add("G", new JsonPrimitive(arg0.getGreen()));
		object.add("B", new JsonPrimitive(arg0.getBlue()));
		
		return object;
	}

}
