package TypeAdapter;

import java.lang.reflect.Type;

import com.google.gson.JsonDeserializationContext;
import com.google.gson.JsonDeserializer;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParseException;
import com.google.gson.JsonPrimitive;
import com.google.gson.JsonSerializationContext;
import com.google.gson.JsonSerializer;

import classes.Pair;



public class PairAdapter implements JsonSerializer<Pair<String, String>>, JsonDeserializer<Pair<String, String>>{

	@Override
	public Pair<String, String> deserialize(JsonElement json, Type typeOfT, JsonDeserializationContext context)
			throws JsonParseException {
		JsonObject object = json.getAsJsonObject();	
		return new Pair<String, String>(object.get("KEY").getAsString(), object.get("VALUE").getAsString());
	}

	@Override
	public JsonElement serialize(Pair<String, String> src, Type typeOfSrc, JsonSerializationContext context) {		
		JsonObject object = new JsonObject();
		object.add("KEY", new JsonPrimitive(src.getKey()));
		object.add("VALUE", new JsonPrimitive(src.getValue()));
		return object;

	}

}
