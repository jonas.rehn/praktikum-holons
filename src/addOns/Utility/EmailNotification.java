package addOns.Utility;
import java.awt.BorderLayout;
import java.awt.ComponentOrientation;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.GridLayout;
import java.util.prefs.*;

import javax.swing.BorderFactory;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JPasswordField;
import javax.swing.JTextField;

import org.apache.commons.mail.*;

import utility.ImageImport;


public class EmailNotification {
	private static Preferences _prefs;
	private static EmailSmtpInformation _info = new EmailSmtpInformation();
	
	
	
	public static class EmailSmtpInformation{
		public String Hostname;
		public int Port;
		public String Username;
		public String Password;
		public String FromEmail;
		public String ToEmail;
	}
	
	
	
	
	
	
	
	public static void main(String[] args) {
		OpenEmailSettings(null);
	}

	public static void OpenEmailSettings(JPanel parent) {
		loadPreferences();
		JFrame frame = new JFrame();
		frame.setTitle("Email Settings");
		frame.setIconImage(ImageImport.loadImage("/Images/Holeg.png",30,30));
		frame.setContentPane(createEditFormular(frame));
		frame.pack();
		frame.setPreferredSize(new Dimension(400,frame.getHeight()));
		frame.pack();
		frame.setVisible(true);
		frame.setLocationRelativeTo(parent);
		frame.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
	}
	
	
	private static JPanel createEditFormular(JFrame frame) {
		JPanel panel = new JPanel();
		panel.setLayout(new BorderLayout());
		panel.setBorder(BorderFactory.createEmptyBorder(0,5,0,5));
		
		JPanel infoPanel = new JPanel();
		GridLayout infoLayout= new GridLayout(0,2);
		infoPanel.add(new JLabel("SMTP Hostname:"));
		JTextField hostnameTextField = new JTextField(_info.Hostname);
		infoPanel.add(hostnameTextField);
		infoPanel.add(new JLabel("Port:"));
		JTextField portTextField = new JTextField(Integer.toString(_info.Port));
		infoPanel.add(portTextField);
		infoPanel.add(new JLabel("Username:"));
		JTextField usernameTextField = new JTextField(_info.Username);
		infoPanel.add(usernameTextField);
		infoPanel.add(new JLabel("Password:"));
		JPasswordField  passwordTextField = new JPasswordField (_info.Password);
		infoPanel.add(passwordTextField);
		infoPanel.add(new JLabel("From Email:"));
		JTextField fromEmailTextField = new JTextField(_info.FromEmail);
		infoPanel.add(fromEmailTextField);
		infoPanel.add(new JLabel("To Email:"));
		JTextField toEmailTextField = new JTextField(_info.ToEmail);
		infoPanel.add(toEmailTextField);
		infoPanel.setLayout(infoLayout);
		panel.add(infoPanel, BorderLayout.CENTER);
		
		JPanel controlPanel = new JPanel();
		controlPanel.setLayout(new FlowLayout(FlowLayout.RIGHT));
		controlPanel.setComponentOrientation(ComponentOrientation.RIGHT_TO_LEFT);
		JButton cancelButton = new JButton("Cancel");
		cancelButton.addActionListener(event -> {
			frame.dispose();
		});
		controlPanel.add(cancelButton);
		JButton applyButton = new JButton("Apply and Close");
		applyButton.addActionListener(event -> {
			// parse Textfields
			_info.Hostname = hostnameTextField.getText();
			_info.Port = Integer.valueOf(portTextField.getText());
			_info.Username = usernameTextField.getText();
			_info.Password =  String.valueOf(passwordTextField.getPassword());
			_info.FromEmail = fromEmailTextField.getText();
			_info.ToEmail = toEmailTextField.getText();
			// Save Preferences
			savePreferences();
			frame.dispose();
		});
		controlPanel.add(applyButton);
		
		JButton testButton = new JButton("Send Test-Email");
		testButton.addActionListener(event -> {
			// parse Textfields
			EmailSmtpInformation testinfo = new EmailSmtpInformation();
			testinfo.Hostname = hostnameTextField.getText();
			testinfo.Port = Integer.valueOf(portTextField.getText());
			testinfo.Username = usernameTextField.getText();
			testinfo.Password = String.valueOf(passwordTextField.getPassword());
			testinfo.FromEmail = fromEmailTextField.getText();
			testinfo.ToEmail = toEmailTextField.getText();
			sendEmail(testinfo, "Holeg Notification Test", "Success.");
		});
		controlPanel.add(testButton);
		
		panel.add(controlPanel, BorderLayout.PAGE_END);
		
		
		
		return panel;
	}
	public static void sendEmail(String subject, String message) {
		loadPreferences();
		sendEmail(_info, subject, message);
	}

	public static void sendEmail(EmailSmtpInformation info, String subject, String message) {
		Email email = new SimpleEmail();
		email.setHostName(info.Hostname);
		email.setSmtpPort(info.Port);
		email.setAuthenticator(new DefaultAuthenticator(info.Username, info.Password));
		email.setSSLOnConnect(true);
		email.setSubject(subject);
		try {
			email.setFrom(info.FromEmail);
			email.setMsg(message);
			email.addTo(info.ToEmail);
			email.send();
		} catch (EmailException e) {
			e.printStackTrace();
		}
	}
	
	private static void savePreferences() {
		_prefs = Preferences.userRoot().node("EmailNotification");
		_prefs.put("Hostname", _info.Hostname);
		_prefs.putInt("Port", _info.Port);
		_prefs.put("Username", _info.Username);
		_prefs.put("Password", _info.Password);
		_prefs.put("FromEmail", _info.FromEmail);
		_prefs.put("ToEmail", _info.ToEmail);
	}
	
	private static void loadPreferences() {
		_prefs = Preferences.userRoot().node("EmailNotification");
		_info.Hostname = _prefs.get("Hostname", "");
		_info.Port = _prefs.getInt("Port", 465);
		_info.Username = _prefs.get("Username", "");
		_info.Password = _prefs.get("Password", "");
		_info.FromEmail= _prefs.get("FromEmail", "");
		_info.ToEmail = _prefs.get("ToEmail", "");
	}
	
	
	
}
