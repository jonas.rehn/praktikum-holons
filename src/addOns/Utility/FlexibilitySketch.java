package addOns.Utility;

public class FlexibilitySketch{
	public double flexChance;
	public float minCost;
	public float maxCost;
	/** The Duration in TimeSteps how long the Flexibility is activated.*/
	public int minDuration;
	public int maxDuration;
	/** The Duration after a successful activation between the next possible activation.*/
	public int minCooldown;
	public int maxCooldown;
	
	
	public void checkValues(){

		minDuration = Math.abs(minDuration);
		maxDuration = Math.abs(maxDuration);
		if(maxDuration < minDuration) {
			//Swap
			int intermediate = minDuration;
			minDuration = maxDuration;
			maxDuration = intermediate;
		}
		minCooldown = Math.abs(minCooldown);
		maxCooldown = Math.abs(maxCooldown);
		if(maxCooldown < minCooldown) {
			//Swap
			int intermediate = minCooldown;
			minCooldown = maxCooldown;
			maxCooldown = intermediate;
		}
		minCost = Math.abs(minCost);
		maxCost = Math.abs(maxCost);
		if(maxCost < minCost) {
			//Swap
			float intermediate = minCost;
			minCost = maxCost;
			maxCost = intermediate;
		}
		flexChance = Math.max(0, Math.min(1, flexChance)); //Clamp
	}
}