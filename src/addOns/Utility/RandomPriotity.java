package addOns.Utility;

import java.util.Hashtable;

import javax.swing.BorderFactory;
import javax.swing.BoxLayout;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JSlider;

import classes.HolonElement.Priority;

public class RandomPriotity extends JPanel{
	/**
	 * Its like a Gradient the Values are on one Axis from Start to End:
	 * |--start--lm--mh--he--end--|
	 * lm: lowMedium
	 * mh: mediumHigh
	 * he: highEssential
	 */
	private int lm = 25 , mh = 50, he = 75;
	private final int start = 0, end = 100;
	
	private JSlider lowChanceSlider = new JSlider(JSlider.HORIZONTAL,0, 100, lm - start);
	private JSlider mediumChanceSlider = new JSlider(JSlider.HORIZONTAL,0, 100, mh - lm);
	private JSlider highChanceSlider = new JSlider(JSlider.HORIZONTAL,0, 100, he - mh);
	private JSlider essentialChanceSlider = new JSlider(JSlider.HORIZONTAL,0, 100, end - he);
	
	
	public RandomPriotity(){
		this.setLayout(new BoxLayout(this, BoxLayout.PAGE_AXIS));
		this.add(initVisualizeSlider(this.lowChanceSlider, "Low"));
		this.add(initVisualizeSlider(this.mediumChanceSlider, "Medium"));
		this.add(initVisualizeSlider(this.highChanceSlider, "High"));
		this.add(initVisualizeSlider(this.essentialChanceSlider, "Essential"));
		lowChanceSlider.setToolTipText("" +((double)lowChanceSlider.getValue()/ 100.0));
		mediumChanceSlider.setToolTipText("" +((double)mediumChanceSlider.getValue()/ 100.0));
		highChanceSlider.setToolTipText("" +((double)highChanceSlider.getValue()/ 100.0));
		essentialChanceSlider.setToolTipText("" +((double)essentialChanceSlider.getValue()/ 100.0));
		lowChanceSlider.addChangeListener(actionEvent ->{
			setLM(lowChanceSlider.getValue());
			updateSliders();
			lowChanceSlider.setToolTipText("" +((double)lowChanceSlider.getValue()/ 100.0));
			System.out.println("lowChance = " +lowChanceSlider.getValue());
		});
		mediumChanceSlider.addChangeListener(actionEvent ->{
			if(lm + mediumChanceSlider.getValue() <= 100) setMH(lm + mediumChanceSlider.getValue());
			else setLM(end - mediumChanceSlider.getValue());
			updateSliders();
			mediumChanceSlider.setToolTipText("" +((double)mediumChanceSlider.getValue()/ 100.0));
			System.out.println("mediumChance = " +  mediumChanceSlider.getValue());
		});
		highChanceSlider.addChangeListener(actionEvent ->{
			if(mh + highChanceSlider.getValue() <= 100) setHE(mh + highChanceSlider.getValue());
			else setMH(end - highChanceSlider.getValue());
			updateSliders();
			highChanceSlider.setToolTipText("" +((double)highChanceSlider.getValue()/ 100.0));
			System.out.println("highChance = " +highChanceSlider.getValue());
		});
		essentialChanceSlider.addChangeListener(actionEvent ->{
			setHE(end - essentialChanceSlider.getValue());
			updateSliders();
			essentialChanceSlider.setToolTipText("" +((double)essentialChanceSlider.getValue()/ 100.0));
			System.out.println("essentialChance = " +essentialChanceSlider.getValue());
		});
	}

	private void setLM(int value) {
		lm = clamp(value, 0, 100);
		if(lm > mh) mh = lm;
		if(lm > he) he = lm;
	}
	
	private void setMH(int value) {
		mh = clamp(value, 0, 100);
		if(mh < lm) lm = mh;
		if(mh > he) he = mh;
	}
	
	private void setHE(int value) {
		he = clamp(value, 0, 100);
		if(he < lm) lm = he;
		if(he < mh) mh = he;
	}
	
	
	private int clamp(int input, int min , int max) {
		return Math.max(min, Math.min(max, input));
	}
	
	
	private void updateSliders(){
		lowChanceSlider.setValue(lm - start);
		mediumChanceSlider.setValue(mh - lm);
		highChanceSlider.setValue(he - mh);
		essentialChanceSlider.setValue(end - he);
	}
	
	

	private JSlider initVisualizeSlider(JSlider jslider, String name) {
		jslider.setBorder(BorderFactory.createTitledBorder(name));
		jslider.setMajorTickSpacing(50);
		jslider.setMinorTickSpacing(5);
		jslider.setPaintTicks(true);
		Hashtable<Integer, JLabel> labelTable = new Hashtable<Integer, JLabel>();
		labelTable.put( Integer.valueOf( 0 ), new JLabel("0.0") );
		labelTable.put( Integer.valueOf( 50 ), new JLabel("0.5") );
		labelTable.put( Integer.valueOf( 100 ), new JLabel("1.0") );
		jslider.setLabelTable( labelTable );
		jslider.setPaintLabels(true);
		return jslider;
	}
	
	public Priority getPriority() {
		double randomDoubleInRange = Math.random() * 100.0;
		if(randomDoubleInRange <= lm) {
			return Priority.Low;
		} else if(randomDoubleInRange <= mh) {
			return Priority.Medium;
		} else if(randomDoubleInRange <= he) {
			return Priority.High;
		} else {
			return Priority.Essential;
		}
	}
	@Override
	public void setEnabled(boolean value) {
		lowChanceSlider.setEnabled(value);
		mediumChanceSlider.setEnabled(value);
		highChanceSlider.setEnabled(value);
		essentialChanceSlider.setEnabled(value);
	}
}
