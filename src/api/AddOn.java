package api;

import javax.swing.JPanel;

import ui.controller.Control;

/**
 * Interface for a Standart Addon.
 * @author Tom Troppmann
 */
public interface AddOn  {
	public JPanel getPanel();
	public void setController(Control control);
}
