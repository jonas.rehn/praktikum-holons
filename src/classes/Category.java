package classes;

import java.util.ArrayList;
import java.util.HashMap;

import com.google.gson.annotations.Expose;

/**
 * Class "Category" performs the functionality of listing elements into groups.
 * Each Category contains an ArrayList of CpsObjects, a name and a HashMap of
 * ObjIdx.
 * 
 * @author Gruppe14
 *
 */

public class Category {
	// objects: is a ArrayList of all Objects that belongs to the Category
	private ArrayList<AbstractCanvasObject> objects;
	// name: is a String chosen by the User
	@Expose
	private String name; 
	// ObjIdx: Index of each Category that corresponds to the order in the tree
	private HashMap<String, Integer> objIdx;

	/**
	 * Category Constructor.
	 * 
	 * @param name name of the Category
	 */
	public Category(String name) {
		setObjects(new ArrayList<AbstractCanvasObject>());
		setName(name);
		setObjIdx(new HashMap<String, Integer>());

	}

	/**
	 * Getter for all CpsObjects.
	 * 
	 * @return the objects
	 */
	public ArrayList<AbstractCanvasObject> getObjects() {
		return objects;
	}

	/**
	 * Set a new ArrayList of CpsObjects.
	 * 
	 * @param objects
	 *            the objects to set
	 */
	public void setObjects(ArrayList<AbstractCanvasObject> objects) {
		this.objects = objects;
	}

	/**
	 * Getter the name of the Category.
	 * 
	 * @return the name
	 */
	public String getName() {
		return name;
	}

	/**
	 * Set the name of the Category to a new one.
	 * 
	 * @param name
	 *            the name to set
	 */
	public void setName(String name) {
		this.name = name;
	}

	/**
	 * Getter of the Objects in the Tree with their respective order.
	 * 
	 * @return the objIdx
	 */
	public HashMap<String, Integer> getObjIdx() {
		return objIdx;
	}

	/**
	 * Set a new sequence of Objects in the tree.
	 * 
	 * @param objIdx
	 *            the objIdx to set
	 */
	public void setObjIdx(HashMap<String, Integer> objIdx) {
		this.objIdx = objIdx;
	}

}
