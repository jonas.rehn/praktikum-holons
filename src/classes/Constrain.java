package classes;

import java.util.function.Predicate;

import com.google.gson.annotations.Expose;

public class Constrain {
	private Predicate<Flexibility> constrainFunction;
	@Expose
	private String name;
	
	public Constrain(Predicate<Flexibility> constrainFunction,String name) {
		this.constrainFunction = constrainFunction;
		this.name = name;
	}
	
	public Predicate<Flexibility> getConstrainFunction() {
		return constrainFunction;
	}
	public String getName() {
		return name;
	}
	
	
	//Example Constrains:
	/** Flexibility should be offered when Element is active.*/
	public static Predicate<Flexibility> onConstrain = f 	-> 	f.getElement().isActive();
	/** Flexibility should be offered when Element is inactive.*/
	public static Predicate<Flexibility> offConstrain = f	-> 	!f.getElement().isActive();
	
	public static Constrain createOnConstrain() {
		return new Constrain( onConstrain, "onConstrain");
	}
	public static Constrain createOffConstrain() {
		return new Constrain( offConstrain, "offConstrain");
	}
	
	
	
	/**
	 * Delete me ....
	 * @return
	 */
	public boolean fixJson() {
		if(name.compareTo("onConstrain") == 0) {
			constrainFunction = onConstrain;
			return false;
		}else if(name.compareTo("offConstrain") == 0){
			constrainFunction = offConstrain;
			return false;
		}else {
			return false;
		}
	}
}
