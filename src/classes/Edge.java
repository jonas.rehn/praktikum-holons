package classes;

import com.google.gson.annotations.Expose;

import java.util.ArrayList;

/**
 * The class "CpsEdge" represents the connections on the GUI. Each connection
 * contains a max. capacity, a flow, a status (isWorking), tags (for internal
 * use of electricity flow), source and destination
 *
 * @author Gruppe14
 */
public class Edge {

    // Max. capacity of the Edge, if flow is greater than the status --> is
    // Working would be false
    @Expose
    private float maxCapacity;
    ArrayList<Integer> tags;
    // for internal use --> flow of electricity (simulation state)
    ArrayList<Integer> pseudoTags;
    // Source
    AbstractCanvasObject a;
    // Destination
    AbstractCanvasObject b;
    
    
    
    
    
    /**
     * Getter for the length of the Cable.
     * Always calculate never saved.
     * Needs to be profiled whats better.
     * @return
     */
    public float getLength() {
    	return (float)a.getPosition().Distance(b.getPosition());
    }

    @Expose
    private boolean breakedManuel = false;
    @Expose
    private boolean unlimitedCapacity = false;
    
    
    /**
     * Constructor without max. capacity (by default as 100)
     *
     * @param a Source
     * @param b Destination
     */
    public Edge(AbstractCanvasObject a, AbstractCanvasObject b) {
        setA(a);
        setB(b);
        if(a == null) {
        	System.out.println("A == NULL");
        }
        if(a == null) {
        	System.out.println("B == NULL");
        }
        this.maxCapacity = 100;
        pseudoTags = new ArrayList<>();
    }

    /**
     * Constructor with a user-defined max. capacity
     *
     * @param a      Source
     * @param b      Destination
     * @param maxCap Maximum Capacity
     */
    public Edge(AbstractCanvasObject a, AbstractCanvasObject b, float maxCap) {
        setA(a);
        setB(b);
        this.maxCapacity = maxCap;
        pseudoTags = new ArrayList<>();
    }

    /**
     * Getter for the max. capacity
     *
     * @return the capacity
     */
    public float getCapacity() {
        return maxCapacity;
    }

    /**
     * Setter for the max. capacity
     *
     * @param cap the Capacity to set
     */
    public void setCapacity(float cap) {
        this.maxCapacity = cap;
    }





    /**
     * Getter for the Source.
     *
     * @return the a
     */
    public AbstractCanvasObject getA() {
        return a;
    }

    /**
     * Set the Source to a new one.
     *
     * @param a the a to set
     */
    public void setA(AbstractCanvasObject a) {
        this.a = a;
    }

    /**
     * Getter for the destination.
     *
     * @return the b
     */
    public AbstractCanvasObject getB() {
        return b;
    }

    /**
     * Set the Destination to a new one.
     *
     * @param b the b to set
     */
    public void setB(AbstractCanvasObject b) {
        this.b = b;
    }





    /**
     * Getter for the ArrayList of tags.
     *
     * @return tags tags for this edge
     */
    public ArrayList<Integer> getTags() {
        return tags;
    }

    /**
     * set the tags into a new set of tags.
     *
     * @param tags tags for this edge
     */
    public void setTags(ArrayList<Integer> tags) {
        this.tags = tags;
    }

    /**
     * Add a new tag to the ArrayList.
     *
     * @param tag tag for the ArrayList
     */
    public void addTag(int tag) {
        if (!tags.contains(tag)) {
            tags.add(tag);
        }
    }

    /**
     * checks whether list contains all given tags
     *
     * @param toCheck tags that are checked
     * @param list    list to be checked
     * @return true if all tags in toCheck are contained in list, false otherwise
     */
    public boolean containsTags(ArrayList<Integer> list, ArrayList<Integer> toCheck) {
        if (toCheck.size() == 0) {
            return true;
        } else {
            for (Integer i : toCheck) {
                if (!(list.contains(i))) {
                    return false;
                }
            }
            return true;
        }
    }

    public void addPseudoTag(int tag) {
        if (!pseudoTags.contains(tag)) {
            pseudoTags.add(tag);
        }
    }

    public void setPseudoTag(ArrayList<Integer> list) {
        pseudoTags = list;
    }

    public ArrayList<Integer> getPseudoTags() {
        return pseudoTags;
    }

    public void recalculateTags() {
        for (Integer i : pseudoTags) {
            if (!tags.contains(i)) {
                tags.add(i);
            }
        }
    }


    
    /**
     * Check if a CpsEdge is Connected to the AbstractCpsObject.
     * @param holonObject the AbstractCpsObject to check.
     * @return true if either a or b is the AbstractCpsObject to check.
     */
    public boolean isConnectedTo(AbstractCanvasObject holonObject)
    {
    	return (holonObject.equals(a) || holonObject.equals(b));
    }
    @Override
    public String toString(){
    	String A = (a == null) ? "null" : a.getName() + "[" + a.getId()+ "]";
    	String B = (b == null) ? "null" : b.getName() + "[" + b.getId()+ "]";
    	return "CpsEdge: " + A + " to " + B;
    }

	public boolean isBreakedManuel() {
		return breakedManuel;
	}

	public void setBreakedManuel(boolean breakedManuel) {
		this.breakedManuel = breakedManuel;
	}

	public boolean isUnlimitedCapacity() {
		return unlimitedCapacity;
	}

	public void setUnlimitedCapacity(boolean unlimitedCapacity) {
		this.unlimitedCapacity = unlimitedCapacity;
	}

}