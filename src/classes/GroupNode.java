package classes;

import com.google.gson.annotations.Expose;

import java.util.ArrayList;
import java.util.HashMap;

public class GroupNode extends AbstractCanvasObject {

	private ArrayList<AbstractCanvasObject> nodes;
	private HashMap<Integer, Integer> nodesIdx;
	
	@Expose
	private int leftBorder; //TODO Delete leftBorder, jet is just set to 0
	@Expose
	private String imgPath = "";
	@Expose
	private int backgroundMode = 0;
	@Expose
	private int backgroundWidth = 0;
	@Expose
	private int backgroundHeight = 0;

	public GroupNode(String nodeName) {
		super(nodeName);
		this.setConnections(new ArrayList<>());
		this.setImage("/Images/upper_node.png");
		this.setSav("CVS");
		this.setId(IdCounter.nextId());
		this.setNodes(new ArrayList<>());
		this.setNodesIdx(new HashMap<>());
	}

	/**
	 * @return the nodes
	 */
	public ArrayList<AbstractCanvasObject> getNodes() {
		return nodes;
	}

	/**
	 * @param nodes
	 *            the nodes to set
	 */
	public void setNodes(ArrayList<AbstractCanvasObject> nodes) {
		this.nodes = nodes;
	}


	/**
	 * @return the nodesIdx
	 */
	public HashMap<Integer, Integer> getNodesIdx() {
		return nodesIdx;
	}

	/**
	 * @param nodesIdx
	 *            the nodesIdx to set
	 */
	public void setNodesIdx(HashMap<Integer, Integer> nodesIdx) {
		this.nodesIdx = nodesIdx;
	}

	public ArrayList<AbstractCanvasObject> getNodesAndGroupnodeNodes(){
		ArrayList<AbstractCanvasObject> nodes = new ArrayList<AbstractCanvasObject>();
		nodes.addAll(getNodes());
		for (AbstractCanvasObject temp : getNodes()) {
			if(temp instanceof GroupNode) {
				nodes.addAll(((GroupNode) temp).getNodesAndGroupnodeNodes());
			}
		}
		return nodes;
	}
	
	
	
	public ArrayList<HolonObject> getNumHolonObj() {
		ArrayList<HolonObject> onlyHolonObj = new ArrayList<HolonObject>();
		for (AbstractCanvasObject temp : getNodes()) {
			if (temp instanceof HolonObject) {
				onlyHolonObj.add((HolonObject) temp);
			}
			if(temp instanceof GroupNode) {
				onlyHolonObj.addAll(((GroupNode) temp).getNumHolonObj());
			}
		}
		return onlyHolonObj;
	}

	public ArrayList<HolonSwitch> getNumSwitches() {
		ArrayList<HolonSwitch> onlySwitsches = new ArrayList<HolonSwitch>();
		for (AbstractCanvasObject temp : getNodes()) {
			if (temp instanceof HolonSwitch) {
				onlySwitsches.add((HolonSwitch) temp);
			}
			if(temp instanceof GroupNode) {
				onlySwitsches.addAll(((GroupNode) temp).getNumSwitches());
			}
		}
		return onlySwitsches;
	}

	public ArrayList<GroupNode> getNumUpperNodes() {
		ArrayList<GroupNode> onlyUpperNodes = new ArrayList<GroupNode>();
		for (AbstractCanvasObject temp : getNodes()) {
			if (temp instanceof GroupNode) {
				onlyUpperNodes.add((GroupNode) temp);
				onlyUpperNodes.addAll(((GroupNode) temp).getNumUpperNodes());
			}
		}
		return onlyUpperNodes;
	}

	public AbstractCanvasObject searchObj(int ID) {
		AbstractCanvasObject result = null;
		for (AbstractCanvasObject obj : getNodes()) {
			if (obj.getId() == ID) {
				result = obj;
				break;
			}
		}
		return result;
	}

	/**
	 * Set the Background Image;
	 * 
	 * @param imagePath
	 *            Image Path
	 * @param mode
	 *            Image Mode
	 * @param width
	 *            Image custom width
	 * @param height
	 *            Image custom height
	 */
	public void setBackgroundImage(String imagePath, int mode, int width, int height) {
		imgPath = imagePath;
		backgroundMode = mode;
		backgroundWidth = width;
		backgroundHeight = height;
	}

	/**
	 * Get the Background Image Path.
	 * 
	 * @return imgPath Path of the Image
	 */
	public String getImagePath() {
		return imgPath;
	}

	/**
	 * Get the Background mode.
	 * 
	 * @return mode the mode
	 */
	public int getBackgroundMode() {
		return backgroundMode;
	}

	/**
	 * Get the Background image Width.
	 * 
	 * @return mode the width of the Image
	 */
	public int getImageWidht() {
		return backgroundWidth;
	}

	/**
	 * Get the Background image Height.
	 * 
	 * @return mode the mode
	 */
	public int getImageHeight() {
		return backgroundHeight;
	}

	/**
	 * @return the leftBorder
	 */
	public int getLeftBorder() {
		return leftBorder;
	}

	/**
	 * @param leftBorder the leftBorder to set
	 */
	public void setLeftBorder(int leftBorder) {
		this.leftBorder = leftBorder;
	}

	@Override
	public AbstractCanvasObject makeCopy() {
		return this;
	}


}
