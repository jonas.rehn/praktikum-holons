package classes;

import com.google.gson.annotations.Expose;

/**
 * Static counter
 * ID-Counter for all Cps Objects.
 * 
 * @author Gruppe14
 */
public class IdCounter {
	@Expose
	private static int counter = 1;

	/**
	 * Return the next ID and increment the ID counter by 1.
	 * @return the next ID
	 */
	public static synchronized int nextId() {
		return counter++;

	}

	/**
	 * Return the Counter.
	 * 
	 * @return the counter
	 */
	public static int getCounter() {
		return counter;
	}

	/**
	 * Set the Counter.
	 * 
	 * @param counter
	 *            the counter to set
	 */
	public static void setCounter(int counter) {
		IdCounter.counter = counter;
	}

	/**
	 * Reset the Counter.
	 */
	public static void resetCounter() {
		counter = 1;
	}
}
