package classes;

import java.util.ArrayList;

/**
 * The class "CpsNode" represents empty Objects in the system. They are just
 * nodes between Objects
 * 
 * @author Gruppe14
 *
 */
public class Node extends AbstractCanvasObject {
	
	/**
	 * Create a new node in the system with an user-defined name.
	 * 
	 * @param objName
	 *            String
	 */
	public Node(String objName) {
		super(objName);
		this.setConnections(new ArrayList<Edge>());
		this.setImage("/Images/node.png");
		this.setSav("CVS");
		this.setId(IdCounter.nextId());
	}
	
	public Node(Node node){
		super(node);
	}
	
	public String toString(){
		return "Node ID:" + super.id;
	}
	
	@Override
	public AbstractCanvasObject makeCopy() {
		return new Node(this);
	}
}
