package classes;

import java.awt.Point;

/**
 * Coordinates of an Object on the canvas with a (int) x-coord and a (int).
 * y-coord
 * 
 * @author Gruppe14
 *
 */
public class Position {
	/** Coordinate*/
	public int x,y;

	/**
	 * Constructor with an positive x and y coord on the canvas.
	 * 
	 * @param x Coord
	 * @param y Coord
	 */
	public Position(int x, int y) {
		this.x = x;
		this.y = y;
	}
	/**
	 * Convert a Point to a Position;
	 * @param p Point
	 */
	public Position(Point p)
	{
		this.x = (int)p.getX();
		this.y = (int)p.getY();
	}
	/**
	 * Default constructor without defined position.
	 */
	public Position() {
		this.x = -1;
		this.y = -1;
	}
	/**
	 * Return the Distance squared to a other Position.
	 * Faster then Distance because no Sqrt() needed.
	 * @param other the other Position.
	 * @return squared distance to the Position
	 */
	public double squareDistance(Position other)
	{
		//The Distance in X
		double xDis = x - other.x;
		//The Distance in Y
		double yDis = y - other.y;
		return xDis * xDis + yDis * yDis;	
	}
	/**
	 * Return the Distance to a other Position.
	 * @param other the other Position.
	 * @return distance to the Position.
	 */
	public double Distance(Position other)
	{
		return Math.sqrt(squareDistance(other));
	}
	
	/**
	 * Clamp the X Value two a upper or lower bound
	 * @param min lower bound
	 * @param max upper bound
	 */
	public void clampX(int min, int max)
	{
		if(x < min) x = min;
		if(x > max) x = max;
	}
	/**
	 * Clamp the Y Value two a upper or lower bound
	 * @param min lower bound
	 * @param max upper bound
	 */
	public void clampY(int min, int max)
	{
		if(y < min) y = min;
		if(y > max) y = max;
	}
	/**
	 * Returns a String that represents the value of this Position.
	 * @return a string representation of this Position.
	 */
	@Override
	public String toString() {
		return "Position[x=" + x + ",y="+ y +"]";
	}
}
