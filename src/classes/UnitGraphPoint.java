package classes;

import java.awt.Point;
import java.awt.geom.Point2D;

/**
 * A class for saving all points of a Object with a editable state.
 * e.g HolonElement, HolonSwitch
 * @author Tom Troppmann
 *
 */
public class UnitGraphPoint {
	/** Relative position in the UnitGraphCanvas */
	public double x,y;
	/** To determine if this point has changed to only write back points that are changed.*/
	public boolean changed;
	/** The displayed Position of the UnitGraphPoint*/
	public Position displayedPosition;
	
	/** Default Constructor */
	public UnitGraphPoint(double x, double y, boolean changed){
		this.x = x;
		this.y = y;
		this.changed = changed;
	}
	/** Constructor with a Point2D.Double*/
	public UnitGraphPoint(Point2D.Double pos, boolean changed){
		this.x = pos.getX();
		this.y = pos.getY();
		this.changed = changed;
	}
	/** Constructor with a Point2D.Double
	 * when changed not specified is false;
	 * */
	public UnitGraphPoint(Point2D.Double pos){
		this.x = pos.getX();
		this.y = pos.getY();
		this.changed = false;
	}
	
	public void calcDisplayedPosition(int border, int widthWithBorder, int heightWithBorder) {
    	//Relativ to Border
    	//1-p.y because its on the Top
		displayedPosition =  new Position((int) (x * widthWithBorder) + border, (int) ((1-y) * heightWithBorder) + border);
    }
	
	public Point.Double getPoint()
	{
		 return new Point2D.Double(x, y);
	}
	@Override
	public String toString() {
		return "[" + x + ":" + y + "]";
	}
}
