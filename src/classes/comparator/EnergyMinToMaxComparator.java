package classes.comparator;

import java.util.Comparator;

import classes.HolonObject;

/**
 * Comparator of the Energy Difference between Total Energy Consumption and Min Consumption
 * of a Holon Objects.
 *
 * @author Andreas T. Meyer-Berg
 */
public class EnergyMinToMaxComparator implements Comparator<HolonObject> {

	private int timeStep = 0;
	
	/**
	 * Comparator for the totalEnergy of HolonObjects
	 * @param timeStep
	 */
	public EnergyMinToMaxComparator(int timeStep) {
		this.timeStep = timeStep;
	}

	@Override
	public int compare(HolonObject o1, HolonObject o2) {
		float minEnergy1 = o1.getMinimumConsumingElementEnergy(timeStep);
		float minEnergy2 = o2.getMinimumConsumingElementEnergy(timeStep);
		float totalEnergy1 = o1.getEnergyAtTimeStep(timeStep);
		float totalEnergy2 = o2.getEnergyAtTimeStep(timeStep);
		float difference1 = totalEnergy1 - minEnergy1;
		float difference2 = totalEnergy2 - minEnergy2;
		if(difference1 < difference2)
			return 1;
		else if (difference1 == difference2) 
			return 0;
		else
			return -1;
	}

}
