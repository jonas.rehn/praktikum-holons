package classes.comparator;

import java.util.Comparator;

import classes.HolonObject;

/**
 * Comparator for Min Consumption of Holon Objects.
 *
 * @author Andreas T. Meyer-Berg
 */
public class MinEnergyComparator implements Comparator<HolonObject> {

	private int timeStep = 0;
	
	/**
	 * Comparator for the minEnergy of HolonObjects
	 * @param timeStep
	 */
	public MinEnergyComparator(int timeStep) {
		this.timeStep = timeStep;
	}

	@Override
	public int compare(HolonObject o1, HolonObject o2) {
		float minEnergy1 = o1.getMinimumConsumingElementEnergy(timeStep);
		float minEnergy2 = o2.getMinimumConsumingElementEnergy(timeStep);
		if(minEnergy1<minEnergy2)
			return 1;
		else if (minEnergy1 == minEnergy2) 
			return 0;
		else
			return -1;
	}

}
