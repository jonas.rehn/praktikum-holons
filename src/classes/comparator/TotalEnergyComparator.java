package classes.comparator;

import java.util.Comparator;

import classes.HolonObject;

/**
 * Comparator for the totalEnergy of HolonObjects
 *
 * @author Andreas T. Meyer-Berg
 */
public class TotalEnergyComparator implements Comparator<HolonObject> {

	private int timeStep = 0;
	
	/**
	 * Comparator for the totalEnergy of HolonObjects
	 * @param timeStep
	 */
	public TotalEnergyComparator(int timeStep) {
		this.timeStep = timeStep;
	}

	@Override
	public int compare(HolonObject o1, HolonObject o2) {
		float totalEnergy1 = o1.getEnergyAtTimeStep(timeStep);
		float totalEnergy2 = o2.getEnergyAtTimeStep(timeStep);
		if(totalEnergy1<totalEnergy2)
			return -1;
		else if (totalEnergy1 == totalEnergy2) 
			return 0;
		else
			return 1;
	}

}
