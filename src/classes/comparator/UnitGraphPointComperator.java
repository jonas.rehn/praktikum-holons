package classes.comparator;

import java.util.Comparator;

import classes.UnitGraphPoint;


public class UnitGraphPointComperator implements Comparator<UnitGraphPoint>{

	@Override
	public int compare(UnitGraphPoint o1, UnitGraphPoint o2) {
		return o1.displayedPosition.x - o2.displayedPosition.x;
	}

}
