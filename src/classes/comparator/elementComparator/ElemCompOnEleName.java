package classes.comparator.elementComparator;

import classes.HolonElement;

public class ElemCompOnEleName extends ElementComparator {

	@Override
	public int compare(HolonElement a, HolonElement b) {

		String eleNameA = a.getEleName();
		String eleNameB = b.getEleName();

		return eleNameA.compareTo(eleNameB);

	}

}
