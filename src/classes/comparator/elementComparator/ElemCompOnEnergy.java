package classes.comparator.elementComparator;

import classes.HolonElement;

/**
 * @author Toni
 * Bei dieser Implementierung wird nach 
 * Energieverbrauch pro HolonElement sortiert
 */
public class ElemCompOnEnergy extends ElementComparator {

	@Override
	public int compare(HolonElement a, HolonElement b) {

		float verbrauchA = a.getEnergyPerElement();
		float verbrauchB = b.getEnergyPerElement();
		if (verbrauchA < verbrauchB)
			return -1;
		if (verbrauchA > verbrauchB)
			return 1;
		return 0;

	}

}
