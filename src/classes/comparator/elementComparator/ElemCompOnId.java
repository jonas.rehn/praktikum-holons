/**
 * 
 */
package classes.comparator.elementComparator;

import classes.HolonElement;

/**
 * @author Toni
 *Comparator zum sortieren nach HolonElementId
 */
public class ElemCompOnId extends ElementComparator {

	@Override
	public int compare(HolonElement a, HolonElement b) {

		float idA = a.getId();
		float idB = b.getId();
		if (idA < idB)
			return -1;
		if (idA > idB)
			return 1;
		return 0;

	}

}
