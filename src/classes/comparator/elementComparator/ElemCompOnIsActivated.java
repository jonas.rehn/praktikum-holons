package classes.comparator.elementComparator;

import classes.HolonElement;

public class ElemCompOnIsActivated  extends ElementComparator {
	
	@Override
	public int compare(HolonElement a, HolonElement b) {

		boolean ifA = a.isActive();
		boolean ifB = b.isActive();
		if (ifA&&!ifB)
			return -1;
		if (!ifA && ifB)
			return 1;
		return 0;

	}

}
