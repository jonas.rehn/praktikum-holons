package classes.comparator.elementComparator;

import classes.HolonElement;

public class ElemCompOnObj extends ElementComparator {

	@Override
	public int compare(HolonElement a, HolonElement b) {

		String objNameA = a.getObjName();
		String objNameB = b.getObjName();
		if(objNameA == null)return -1;
		if (objNameB == null)return 1;
		return objNameA.compareTo(objNameB);

	}

}
