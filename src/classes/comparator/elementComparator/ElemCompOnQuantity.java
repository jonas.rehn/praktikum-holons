package classes.comparator.elementComparator;

import classes.HolonElement;

public class ElemCompOnQuantity extends ElementComparator {

	@Override
	public int compare(HolonElement a, HolonElement b) {

		float amountA = a.getAmount();
		float amountB = b.getAmount();
		if (amountA < amountB)
			return -1;
		if (amountA > amountB)
			return 1;
		return 0;

	}

}
