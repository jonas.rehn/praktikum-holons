package classes.comparator.elementComparator;

import java.util.Comparator;

import classes.HolonElement;

/**
 * @author Toni
 *Eine abstrakte Klasse um bei HolonObjekten den Comparator 
 *der die HolonElemente sortiert welchseln zu k�nnen
 */
public abstract class ElementComparator implements Comparator<HolonElement> {
	
	public abstract int compare(HolonElement a, HolonElement b);
	
}
	
