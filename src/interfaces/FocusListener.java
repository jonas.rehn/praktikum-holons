package interfaces;


public interface FocusListener extends java.awt.event.FocusListener{
	@Override
	public default void focusGained (java.awt.event.FocusEvent evt) {
    	//Do nothing
    }
	//focusLost has to be defined in the lambda expression 
}
