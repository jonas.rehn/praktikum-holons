package interfaces;

import java.awt.geom.Point2D;
import java.util.LinkedList;

/**
 * Interface for all Elements that have a Graph to edit it state over time.
 * @author Tom Troppmann
 *
 */
public interface GraphEditable {

	/**
	 * all types of graphs
	 */
	public static enum Graphtype {
		boolGraph, doubleGraph
	}
	/**
	 * Determine what type the Graph have.
	 * @return the type of the Graph
	 */
	Graphtype getGraphType();
	/**
	 * Getter for the graph.
	 * @return The list of all graph points.
	 */
	LinkedList<Point2D.Double> getStateGraph();
	
	/**
	 * Sample the Graph on the object.
	 */
	void sampleGraph();
	
	/**
	 * Resets the Graph two the initial start
	 * e.g. the Point Left and Right at 100%
	 */
	void reset();
}
