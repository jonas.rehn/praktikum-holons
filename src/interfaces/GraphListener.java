package interfaces;

import classes.AbstractCanvasObject;

import java.util.ArrayList;

public interface GraphListener {

    void repaintTree();

    void addTrackedObject(ArrayList<AbstractCanvasObject> hlList);
}
