package interfaces;

public interface LocalMode {
	
	public static final int STANDARD_GRAPH_ACCURACY=100;
	/**
	 * Sets the local period of the element. 
	 * If the simulation has 100 steps and the local period is 50,
	 * then there will be two full cycles during the simulation.
	 * @param period The local period for this element.
	 */
	void setLocalPeriod(int period);
	
	/**
	 * Used to query the local period of an element.
	 * @return The local period of this element.
	 * This should return the set local period,
	 * which may be anything, even if the component is set to be "stretching",
	 * that is,
	 * acting as if the local period was the same
	 * as the number of total iterations for this simulation.
	 */
	int getLocalPeriod();
	
	/**
	 * @return true when using his own local Period.
	 */
	boolean isUsingLocalPeriod();
	
	/**
	 *  Determine if it should use his own LocalPeriod or use the global Period over the entire simulation.
	 * 	Local Period is opposed to repeated once the period is over.
	 */
	void setUseLocalPeriod(boolean state);
}
