package interfaces;

import java.util.ArrayList;

/**
 * Intercae for ObjectListener.
 * 
 * @author Gruppe14
 */
public interface ObjectListener {
	/**
	 * Constructor.
	 * 
	 * @param objects
	 *            AbstractCpsObject
	 */
	public void onChange(ArrayList<classes.AbstractCanvasObject> objects);
}
