package ui.controller;

import ui.model.Model;

/**
 * Autosave Controller.
 * 
 * @author Gruppe14
 */
public class AutoSaveController {
	private Model model;
	private int max = Integer.MAX_VALUE;
	private int numberOfSaves = 20;
	private int currentSave;
	private int count = 0;
	private boolean isAllowed = false;

	/**
	 * Constructor.
	 * 
	 * @param model
	 *            the Model
	 */
	public AutoSaveController(Model model) {
		this.model = model;
	}

	/**
	 * Increase the Auto save number.
	 */
	public void increaseAutoSaveNr() {
		currentSave = model.getAutoSaveNr() + 1;
		if (count < currentSave) {
			count = currentSave;
		}
		if (numberOfSaves == count) {
			isAllowed = true;
		}
		if (currentSave > max) {
			currentSave = 0;
		}

		model.setAutoSaveNr(currentSave);
	}

	/**
	 * Decrease the Autosave Number.
	 */
	public void decreaseAutoSaveNr() {
		currentSave = model.getAutoSaveNr() - 1;
		// if (currentSave < 0) {
		// currentSave = max;
		// }
		model.setAutoSaveNr(currentSave);
	}

	/**
	 * Return the Autosave Number.
	 * 
	 * @return Autosave Number
	 */
	public int getAutoSaveNr() {
		return model.getAutoSaveNr();
	}

	/**
	 * Return isAllowed.
	 * 
	 * @return isAllowed
	 */
	public boolean allowed() {
		return isAllowed;
	}
}
