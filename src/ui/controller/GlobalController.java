package ui.controller;

import ui.model.Model;
import ui.model.Model.FairnessModel;

/**
 * Controller for the Global Variables.
 * 
 * @author Gruppe14
 */
public class GlobalController {

	private Model model;

	/**
	 * Constructor.
	 * 
	 * @param model
	 *            the Model
	 */
	public GlobalController(Model model) {
		this.model = model;
	}

	/**
	 * Returns SCALE.
	 * 
	 * @return SCALE
	 */
	public int getScale() {
		return model.getScale();
	}

	/**
	 * Returns SCALE Divided by 2.
	 * 
	 * @return SCALE Divided by 2
	 */
	public int getScaleDiv2() {
		return model.getScaleDiv2();
	}

	/**
	 * Changes the value of SCALE and SCALEDIV2.
	 * 
	 * @param s
	 *            Scale
	 */
	public void setScale(int s) {
		model.setScale(s);
	}

	/**
	 * sets the current Iteration.
	 * 
	 * @param curit
	 *            the current Iteration
	 */
	public void setCurIteration(int curit) {
		model.setCurIteration(curit);
	}

	/**
	 * Returns numberOfSaves.
	 * 
	 * @return numberOfSaves
	 */
	public int getNumbersOfSaves() {
		return model.getNumberOfSaves();
	}

	/**
	 * sets the max number of autosaves.
	 * 
	 * @param numberofSaves
	 *            the max number of autosaves
	 */
	public void setNumberOfSaves(int numberofSaves) {
		model.setNumberOfSaves(numberofSaves);
		;
	}

	/**
	 * Set the timerSpeed.
	 * @param t
	 *            Interval in ms
	 */
	public void setTimerSpeed(int t) {
		model.setTimerSpeed(t);
	}

	/**
	 * Set the Canvas X Size.
	 * 
	 * @param canvasX the cANVAS_X to set
	 */
	public void setCanvasX(int canvasX) {
		model.setCanvasX(canvasX);
	}


	/**
	 * Set the Canvas Y Size.
	 * 
	 * @param canvasY the cANVAS_Y to set
	 */
	public void setCanvasY(int canvasY) {
		model.setCanvasY(canvasY);
	}
	
	
	
	public void setMaxCapacity(float cap) {
		model.setMaxCapacity(cap);
	}


	
	/**
	 * sets showSupplyBars
	 * @param showSupplyBars
	 */
	public void setShowSupplyBars(boolean showSupplyBars) {
		model.setShowSupplyBars(showSupplyBars);
	}
	
	/**
	 * sets showSupplyBars
	 * @param showSupplyBars
	 */
	public void setFairnessModel(FairnessModel fairnessModel) {
		model.setFairnessModel(fairnessModel);
	}
}
