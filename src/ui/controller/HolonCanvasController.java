package ui.controller;

import classes.*;
import ui.model.Model;

import java.awt.*;
import java.util.ArrayList;

public class HolonCanvasController {

	// Ball objects
	private ArrayList<HolonBody> bodies = new ArrayList<>();
	private int subCount;
	private Dimension center;
	private Model model;
	private ArrayList<HolonBody> sortedSize = new ArrayList<>();
	private ArrayList<HolonBody> sortedDist = new ArrayList<>();
	private int toDrag;
	private boolean beingDragged;

	/**
	 * Constructor.
	 * 
	 * @param model
	 *            the Model
	 * @param mp
	 *            the MultipurposeController
	 */
	public HolonCanvasController(Model model) {
		this.model = model;
	}

	// sort the HolonBodys after size
	public void rearrangeAfterSize() {
		int j = 0;
		sortedSize.addAll(bodies);
		insertionSizeSort(sortedSize);
		sortedDist.addAll(bodies);

		ArrayList<Vector2d> pos = insertionDistSort(sortedDist);
		for (int i = 0; i < subCount; i++) {
			int ID = sortedSize.get(subCount - 1 - i).getId();
			for (j = 0; j < subCount; j++) {
				if (ID == bodies.get(j).getId()) {
					bodies.get(j).position = pos.get(i);
					break;
				}
			}
		}
	}

	public int getActiveElements(ArrayList<AbstractCanvasObject> objects) {
		int val = 0;
		for (AbstractCanvasObject obj : objects) {
			if (obj instanceof HolonObject) {
				for (HolonElement ele : ((HolonObject) obj).getElements()) {
                    if (ele.isActive()) {
                        val += 1;
					}
				}
			} else if (obj instanceof GroupNode) {
				val += getTotalProduction(((GroupNode) obj).getNodes());
			}
		}
		return val;
	}

	public int getTotalProducers(ArrayList<AbstractCanvasObject> objects) {
		float val = 0;
		int prod = 0;
		int tStep = model.getCurIteration();
		for (AbstractCanvasObject obj : objects) {
			if (obj instanceof HolonObject) {
				for (HolonElement ele : ((HolonObject) obj).getElements()) {
                    if (ele.getEnergyAtTimeStep(tStep) > 0 && ele.isActive()) {
                        val += ele.getEnergyAtTimeStep(tStep) * ele.getAmount();
                    }
                }
				if (val > 0)
					prod += 1;
			} else if (obj instanceof GroupNode) {
				val += getTotalProduction(((GroupNode) obj).getNodes());
			}
		}
		return prod;
	}

	public int getTotalElements(ArrayList<AbstractCanvasObject> objects) {
		int val = 0;
		for (AbstractCanvasObject obj : objects) {
			if (obj instanceof HolonObject) {
				val += ((HolonObject) obj).getElements().size();
			} else if (obj instanceof GroupNode) {
				val += getTotalConsumption(((GroupNode) obj).getNodes());
			}
		}
		return val;
	}

	public float getTotalConsumption(ArrayList<AbstractCanvasObject> objects) {
		float val = 0;
		int tStep = model.getCurIteration();
		for (AbstractCanvasObject obj : objects) {
			if (obj instanceof HolonObject) {
				for (HolonElement ele : ((HolonObject) obj).getElements()) {
                    if (ele.getEnergyAtTimeStep(tStep) < 0 && ele.isActive()) {
                        val += ele.getEnergyAtTimeStep(tStep) * ele.getAmount();
                    }
                }
			} else if (obj instanceof GroupNode) {
				val += getTotalConsumption(((GroupNode) obj).getNodes());
			}
		}
		return val;
	}

	public float getTotalProduction(ArrayList<AbstractCanvasObject> objects) {
		float val = 0;
		int tStep = model.getCurIteration();
		for (AbstractCanvasObject obj : objects) {
			if (obj instanceof HolonObject) {
				for (HolonElement ele : ((HolonObject) obj).getElements()) {
                    if (ele.getEnergyAtTimeStep(tStep) > 0 && ele.isActive()) {
                        val += ele.getEnergyAtTimeStep(tStep) * ele.getAmount();
                    }
                }
			} else if (obj instanceof GroupNode) {
				val += getTotalProduction(((GroupNode) obj).getNodes());
			}
		}
		return val;
	}

	public void updateBodies(float elapsedSeconds) {
		// step the position of movable objects based off their velocity/gravity
		// and elapsedTime
		for (int i = 0; i < subCount; i++) {
			if (toDrag != bodies.get(i).getId() || !beingDragged) {
				bodies.get(i).position.setX(
						(float) (bodies.get(i).position.getX() + (bodies.get(i).velocity.getX() * (elapsedSeconds))
								- ((bodies.get(i).position.getX() - center.getWidth()) / (50 + subCount))));
				bodies.get(i).position.setY(
						(float) (bodies.get(i).position.getY() + (bodies.get(i).velocity.getY() * (elapsedSeconds))
								- ((bodies.get(i).position.getY() - center.getHeight()) / (50 + subCount))));
				float epsilon = 0.000009f;
				if (Math.abs(bodies.get(i).velocity.getX()) < epsilon)
					bodies.get(i).velocity.setX(0);
				if (Math.abs(bodies.get(i).velocity.getY()) < epsilon)
					bodies.get(i).velocity.setY(0);
			}
		}
		checkCollisions();
	}

	// Insertion sort for Sweep and Prune
	public void insertionSort(ArrayList<HolonBody> a) {
		for (int p = 1; p < subCount; p++) {
			Comparable<HolonBody> tmp = a.get(p);
			int j = p;

			for (; j > 0 && tmp.compareTo(a.get(j - 1)) < 0; j--)
				a.set(j, a.get(j - 1));

			a.set(j, (HolonBody) tmp);
		}
	}

	// Insertion sort for subnet size
	private void insertionSizeSort(ArrayList<HolonBody> a) {
		for (int p = 1; p < subCount; p++) {
			HolonBody tmp = a.get(p);
			int j = p;

			for (; j > 0 && tmp.compareSizeTo(a.get(j - 1)) < 0; j--)
				a.set(j, a.get(j - 1));

			a.set(j, (HolonBody) tmp);
		}
	}

	// Insertion sort for HolonBody distance
	private ArrayList<Vector2d> insertionDistSort(ArrayList<HolonBody> a) {
		ArrayList<Vector2d> pos = new ArrayList<>();
		for (int p = 1; p < subCount; p++) {
			HolonBody tmp = a.get(p);
			int j = p;

			for (; j > 0 && tmp.compareDistTo(a.get(j - 1), center) < 0; j--)
				a.set(j, a.get(j - 1));

			a.set(j, (HolonBody) tmp);
		}
		for (int i = 0; i < subCount; i++)
			pos.add(a.get(i).position);
		return pos;
	}

	public void checkCollisions() {
		insertionSort(bodies);

		for (int i = 0; i < subCount; i++) {
			// Ball to Ball collision
			float radiusI = bodies.get(i).getRadius();
			float xPlusradius = (bodies.get(i).position.getX() + radiusI);
			float yPlusradius = (bodies.get(i).position.getY() + radiusI);
			float yMinusRadius = (bodies.get(i).position.getY()- radiusI);
			for (int j = i + 1; j < subCount; j++) {
				float radiusJ = bodies.get(j).getRadius();
				if (xPlusradius < (bodies.get(j).position.getX()- radiusJ))
					break;

				if (yPlusradius < (bodies.get(j).position.getY()- radiusJ)
					|| (bodies.get(j).position.getY() + radiusJ) < yMinusRadius)
					continue;

				bodies.get(i).resolveCollision(bodies.get(j));

			}
		}

	}

	public ArrayList<HolonBody> getBodies() {
		return bodies;
	}

	public void setDrag(boolean beingDragged) {
		this.beingDragged = beingDragged;
	}

	public void setBodyToDrag(int i) {
		this.toDrag = i;
	}

}
