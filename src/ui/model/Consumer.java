package ui.model;

import java.util.ArrayList;

import classes.HolonObject;

public class Consumer extends DecoratedHolonObject {

	private ArrayList<SupplierListEntry> supplierList = new ArrayList<SupplierListEntry>();
	private float energyFromNetwork;
	private float minimumConsumingElementEnergy;
	private float energyNeededFromNetwork;
	private float energyFromConsumingElemnets;
	private float energySelfSupplied;
	public Consumer(HolonObject objectToLookAt) {
		super(objectToLookAt);
		energyNeededFromNetwork = 0.0f;
	}

	@Override
	float getEnergy() {
		return energyFromNetwork-energyNeededFromNetwork;
	}
	public float getEnergyFromNetwork() {
		return energyFromNetwork;
	}
	public void setEnergyFromNetwork(float energyFromNetwork) {
		this.energyFromNetwork = energyFromNetwork;
	}

	public float getMinimumConsumingElementEnergy() {
		return minimumConsumingElementEnergy;
	}

	public void setMinimumConsumingElementEnergy(float minimumConsumingElementEnergy) {
		this.minimumConsumingElementEnergy = minimumConsumingElementEnergy;
	}

	public ArrayList<SupplierListEntry> getSupplierList() {
		return supplierList;
	}

	public void setSupplierList(ArrayList<SupplierListEntry> supplierList) {
		this.supplierList = supplierList;
	}

	public float getEnergyNeededFromNetwork() {
		return energyNeededFromNetwork;
	}

	public void setEnergyNeededFromNetwork(float energyNeededFromNetwork) {
		this.energyNeededFromNetwork = energyNeededFromNetwork;
	}
	@Override
	public String toString() {
		return getModel().getName() + ":" + getModel().getId()  + ", Energy: "+ getEnergy() +
				" [" + (energyFromNetwork + energySelfSupplied) +"/" + energyFromConsumingElemnets +"]";	
	}
	public float getEnergyFromConsumingElemnets() {
		return energyFromConsumingElemnets;
	}

	public void setEnergyFromConsumingElemnets(float energyFromConsumingElemnets) {
		this.energyFromConsumingElemnets = energyFromConsumingElemnets;
	}
	public float getEnergySelfSupplied() {
		return energySelfSupplied;
	}

	public float getSupplyBarPercentage() {
//		double test = (getEnergyFromConsumingElemnets() > 0.001) ? (getEnergyFromNetwork()+ this.getEnergySelfSupplied())/getEnergyFromConsumingElemnets() : 1.0f;
//		System.out.println("SupplyBar = [" +getEnergyFromConsumingElemnets() + "] is " + test);
//		return (float) test;
		return (getEnergyFromConsumingElemnets() > 0.001) ? (getEnergyFromNetwork()+ this.getEnergySelfSupplied())/getEnergyFromConsumingElemnets() : 1.0f;
	}
	public void setEnergySelfSupplied(float energySelfSupplied) {
		this.energySelfSupplied = energySelfSupplied;
	}
	public class SupplierListEntry{
		public Supplier supplier;
		public float energyFromSupplier;
		public SupplierListEntry(Supplier supplier,float energyFromSupplier) {
			this.supplier = supplier;
			this.energyFromSupplier = energyFromSupplier;
		}
	}

}
