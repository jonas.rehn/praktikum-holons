package ui.model;

import classes.Edge;

public class DecoratedCable {
	public enum CableState{
		Working, Burned
	}
	private Edge model;
	private CableState state;
	private float flowEnergy;
	public DecoratedCable(Edge edge, CableState state, float flowEnergy){
		this.model = edge;
		this.state = state;
		this.flowEnergy = flowEnergy;
	}
	public Edge getModel() {
		return model;
	}
	public CableState getState() {
		return state;
	}
	public float getFlowEnergy() {
		return flowEnergy;
	}
}
