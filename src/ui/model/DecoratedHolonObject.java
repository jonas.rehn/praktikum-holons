package ui.model;

import classes.HolonObject;

public abstract class DecoratedHolonObject {
	public enum HolonObjectState {
		NO_ENERGY, NOT_SUPPLIED, SUPPLIED, PRODUCER, PARTIALLY_SUPPLIED, OVER_SUPPLIED
	}
	private HolonObject model;
	private HolonObjectState state;
	public DecoratedHolonObject(HolonObject objectToLookAt){
		model = objectToLookAt;
	}
	abstract float getEnergy();
	public HolonObject getModel() {
		return model;
	}
	public HolonObjectState getState() {
		return state;
	}
	public void setState(HolonObjectState state) {
		this.state = state;
	}
}
