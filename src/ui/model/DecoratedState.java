package ui.model;

import java.util.ArrayList;

import ui.controller.FlexManager;


public class DecoratedState {
	int timestepOfState;
	ArrayList<DecoratedNetwork> networkList;
	ArrayList<DecoratedCable> leftOverEdges;
	ArrayList<DecoratedSwitch> decoratedSwitches;
	FlexManager flexManager;
	public DecoratedState(ArrayList<DecoratedNetwork> networkList, ArrayList<DecoratedCable> leftOverEdges, ArrayList<DecoratedSwitch> decoratedSwitches, FlexManager flexManager , int timestepOfState){
		this.networkList = networkList;
		this.leftOverEdges = leftOverEdges;
		this.decoratedSwitches = decoratedSwitches;
		this.timestepOfState = timestepOfState;
		this.flexManager = flexManager;
	}
	public ArrayList<DecoratedNetwork> getNetworkList() {
		return networkList;
	}
	public ArrayList<DecoratedCable> getLeftOverEdges() {
		return leftOverEdges;
	}
	public ArrayList<DecoratedSwitch> getDecoratedSwitches() {
		return decoratedSwitches;
	}
	public FlexManager getFlexManager() {
		return flexManager;
	}
	public int getTimestepOfState() {
		return timestepOfState;
	}
}
