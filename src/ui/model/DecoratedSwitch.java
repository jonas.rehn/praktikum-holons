package ui.model;

import classes.HolonSwitch;

public class DecoratedSwitch {
	public enum SwitchState{
		Closed, Open
	}
	private SwitchState state;
	private HolonSwitch model;
	public DecoratedSwitch(HolonSwitch hSwitch, SwitchState state){
		this.model = hSwitch;
		this.state = state;
	}
	public SwitchState getState() {
		return state;
	}

	public HolonSwitch getModel() {
		return model;
	}

	
}
