package ui.model;

import classes.AbstractCanvasObject;
/**
 * For the Visual State.
 * @author Tom
 *
 */
public class ExitCable {
	//Classification 
	public enum ExitCableState {
		UP, DOWN, DOWNUP, DOWNDOWN
	}
	private ExitCableState state;
	//GroupNode or AbstractCpsObject
	private AbstractCanvasObject start;
	private AbstractCanvasObject finish;
	private DecoratedCable cable;
	
	public ExitCable(ExitCableState state, AbstractCanvasObject start, AbstractCanvasObject finish, DecoratedCable cable) {
		this.state = state;
		this.start = start;
		this.finish = finish;
		this.cable = cable;
	}

	public ExitCableState getState() {
		return state;
	}
	public AbstractCanvasObject getStart() {
		return start;
	}
	public AbstractCanvasObject getFinish() {
		return finish;
	}
	public DecoratedCable getCable() {
		return cable;
	}

}
