package ui.model;

import classes.Edge;
import classes.HolonObject;
import ui.controller.FlexManager;
import ui.model.DecoratedCable.CableState;

/**
 * Intermediate to calculate/simulate the burning of Cables.
 * When all burning is done the Cable will be represented as a DecoratedCable.
 * @see DecoratedCable
 */
public class IntermediateCableWithState {
	private CableState state;
	private Edge model;
	public IntermediateCableWithState(Edge model,CableState state)
	{
		this.model = model;
		this.state = state;
	}
	public CableState getState() {
		return state;
	}
	public void setState(CableState state) {
		this.state = state;
	}
	public Edge getModel() {
		return model;
	}
	//ugly
	public float getEnergyFromConnetedAtTimestep(int iteration, FlexManager flexManager) {
		float energy = 0.0f;
		if(model.getA() instanceof HolonObject && ((HolonObject) model.getA()).getEnergyAtTimeStepWithFlex(iteration, flexManager) > 0) energy += ((HolonObject) model.getA()).getEnergyAtTimeStepWithFlex(iteration, flexManager);
		if(model.getB() instanceof HolonObject && ((HolonObject) model.getB()).getEnergyAtTimeStepWithFlex(iteration, flexManager) > 0) energy += ((HolonObject) model.getB()).getEnergyAtTimeStepWithFlex(iteration, flexManager);
		return energy;
	}
	
}
