package ui.model;

import classes.AbstractCanvasObject;
import classes.Edge;
import classes.GroupNode;

/**
 * A VisualRepresentation to represent a connection from a UpperNode to a AbstactCpsObject, but actually the UpperNode is not connected.
 * @author Tom
 *
 */
public class IntermediateCalculationCable {
	private AbstractCanvasObject outsideObject;
	private AbstractCanvasObject insideObject;
	private GroupNode getInsideUpperNode;
	private GroupNode getOutsideUpperNode;
	private DecoratedCable cable;
	public IntermediateCalculationCable(DecoratedCable cable, GroupNode getInsideUpperNode,GroupNode getOutsideUpperNode, AbstractCanvasObject insideObject, AbstractCanvasObject outsideObject){
		this.cable = cable;
		this.getInsideUpperNode = getInsideUpperNode;
		this.getOutsideUpperNode = getOutsideUpperNode;
		this.insideObject = insideObject;
		this.outsideObject = outsideObject;
	}
	public AbstractCanvasObject getOusideObject() {
		return outsideObject;
	}
	public AbstractCanvasObject getInsideObject() {
		return insideObject;
	}
	public GroupNode getInsideUpperNode() {
		return getInsideUpperNode;
	}
	public DecoratedCable getCable() {
		return cable;
	}
	public Edge getModel() {
		return cable.getModel();
	}
	public GroupNode getOutsideUpperNode() {
		return getOutsideUpperNode;
	}
	public String toString() {
		return getModel().toString();
		
	}

}
