package ui.model;

import java.util.ArrayList;
import java.util.HashMap;

import classes.AbstractCanvasObject;
import classes.Edge;
import classes.Node;
import classes.GroupNode;
import classes.HolonObject;
import classes.HolonSwitch;
import ui.model.DecoratedCable.CableState;

/**
 * For DecoratedState And VisualRepresentationalState
 * @author Tom
 *
 */
public class MinimumModel {
	
	
	
	
	private ArrayList<HolonObject> holonObjectList = new ArrayList<HolonObject>();
	private ArrayList<IntermediateCableWithState> cableList = new ArrayList<IntermediateCableWithState>();
	private ArrayList<Node> nodeList = new ArrayList<Node>();
	private ArrayList<HolonSwitch> switchList = new ArrayList<HolonSwitch>();
	//-->Only for Visual:
	private ArrayList<GroupNode> uppderNodeList = new ArrayList<GroupNode>();
	private HashMap<AbstractCanvasObject, GroupNode> inGroupObjects = new HashMap<>();
	HashMap<Edge, ArrayList<GroupNode>> inGroupEdges = new HashMap<Edge, ArrayList<GroupNode>>();
	//<--
	
	
	
	
	
	public HashMap<Edge, ArrayList<GroupNode>> getInGroupEdges() {
		return inGroupEdges;
	}

	public MinimumModel(ArrayList<AbstractCanvasObject> abstractObjectList, ArrayList<Edge> edgeList) {// Constructor because of old Model
		for (AbstractCanvasObject aCps : abstractObjectList) {
			if (aCps instanceof HolonObject) {
				holonObjectList.add((HolonObject) aCps);
			}
			else if (aCps instanceof Node) nodeList.add((Node) aCps);
			else if (aCps instanceof HolonSwitch) switchList.add((HolonSwitch) aCps);
			else if(aCps instanceof GroupNode) {
				addUpperObjects((GroupNode)aCps);
				uppderNodeList.add((GroupNode)aCps);
			}
		}
		for (Edge edge : edgeList) {
			this.cableList.add(new IntermediateCableWithState(edge, CableState.Working));
			AbstractCanvasObject objectA = edge.getA();
			AbstractCanvasObject objectB = edge.getB();
			if(inGroupObjects.containsKey(objectA) || inGroupObjects.containsKey(objectB)){
				ArrayList<GroupNode> list = new ArrayList<GroupNode>(); 
				if(inGroupObjects.containsKey(objectA)) {
					list.add(inGroupObjects.get(objectA));
				}
				if(inGroupObjects.containsKey(objectB)) {
					list.add(inGroupObjects.get(objectB));					
				}
				inGroupEdges.put(edge, list);
			}
		}
	}

	private void addUpperObjects(GroupNode aUpperNode) {
		for(AbstractCanvasObject aCps : aUpperNode.getNodes()) {
			if (aCps instanceof HolonObject) holonObjectList.add((HolonObject) aCps);
			else if (aCps instanceof Node) nodeList.add((Node) aCps);
			else if (aCps instanceof HolonSwitch) switchList.add((HolonSwitch) aCps);
			else if(aCps instanceof GroupNode) {
				addUpperObjects((GroupNode)aCps);
				uppderNodeList.add((GroupNode)aCps);
			}
			inGroupObjects.put(aCps, aUpperNode);
		}
	}
	
	public ArrayList<HolonObject> getHolonObjectList() {
		return holonObjectList;
	}
	public void setHolonObjectList(ArrayList<HolonObject> holonObjectList) {
		this.holonObjectList = holonObjectList;
	}
	public ArrayList<IntermediateCableWithState> getEdgeList() {
		return cableList;
	}
	public void setEdgeList(ArrayList<IntermediateCableWithState> cableList) {
		this.cableList = cableList;
	}
	public ArrayList<Node> getNodeList() {
		return nodeList;
	}
	public void setNodeList(ArrayList<Node> nodeList) {
		this.nodeList = nodeList;
	}
	public ArrayList<HolonSwitch> getSwitchList() {
		return switchList;
	}
	public void setSwitchList(ArrayList<HolonSwitch> switchList) {
		this.switchList = switchList;
	}

	public ArrayList<GroupNode> getUppderNodeList() {
		return uppderNodeList;
	}

	public HashMap<AbstractCanvasObject, GroupNode> getInGroupObjects() {
		return inGroupObjects;
	}

	
}
