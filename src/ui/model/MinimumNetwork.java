package ui.model;

import java.util.ArrayList;

import classes.AbstractCanvasObject;
import classes.HolonObject;

public class MinimumNetwork {
	private ArrayList<HolonObject> holonObjectList = new ArrayList<HolonObject>();
	private ArrayList<IntermediateCableWithState> edgeList = new ArrayList<IntermediateCableWithState>();
	//ToCalculate average path
	private ArrayList<AbstractCanvasObject> nodeAndSwitches = new ArrayList<AbstractCanvasObject>();
	public MinimumNetwork(ArrayList<HolonObject> holonObjectList, ArrayList<IntermediateCableWithState> edgeList){
		this.holonObjectList = holonObjectList;
		this.edgeList = edgeList;
	}
	public ArrayList<HolonObject> getHolonObjectList() {
		return holonObjectList;
	}
	public ArrayList<IntermediateCableWithState> getEdgeList() {
		return edgeList;
	}
	public String toString()
	{
		String objecte = "[";
		for(HolonObject object :holonObjectList) {
			objecte += " " + object.getObjName();
		}
		objecte += "]";
		String edges = "[";
		for(IntermediateCableWithState edge :edgeList) {
			edges += " " + edge.getModel();
		}
		edges += "]";
		return objecte + edges;
	}
	public ArrayList<AbstractCanvasObject> getNodeAndSwitches() {
		return nodeAndSwitches;
	}
}
