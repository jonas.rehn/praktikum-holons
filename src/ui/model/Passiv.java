package ui.model;

import classes.HolonObject;

public class Passiv extends DecoratedHolonObject {
	public Passiv(HolonObject objectToLookAt) {
		super(objectToLookAt);
	}
	
	@Override
	float getEnergy() {
		return 0;
	}

}
