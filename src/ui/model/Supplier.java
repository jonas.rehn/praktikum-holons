package ui.model;

import java.util.ArrayList;

import classes.HolonObject;

public class Supplier extends DecoratedHolonObject {

	private ArrayList<ConsumerListEntry> consumerList = new ArrayList<ConsumerListEntry>();
	private float energyToSupplyNetwork;
	private float energySupplied;
	private float energySelfConsuming;
	public Supplier(HolonObject objectToLookAt, float energyToSupplyNetwork, float energySelfConsuming) {
		super(objectToLookAt);
		this.energyToSupplyNetwork = energyToSupplyNetwork;
		energySupplied = 0.0f;
		this.energySelfConsuming = energySelfConsuming;
	}

	@Override
	float getEnergy() {
		return energyToSupplyNetwork;
	}

	public ArrayList<ConsumerListEntry> getConsumerList() {
		return consumerList;
	}

	public void setConsumerList(ArrayList<ConsumerListEntry> consumerList) {
		this.consumerList = consumerList;
	}

	public float getEnergyToSupplyNetwork() {
		return energyToSupplyNetwork;
	}

	
	public float getEnergySelfConsuming() {
		return energySelfConsuming;
	}
	
	public float getEnergyProducing() {
		return energyToSupplyNetwork + energySelfConsuming;
	}
	
	public float getEnergySupplied() {
		return energySupplied;
	}

	public void setEnergySupplied(float energySupplied) {
		this.energySupplied = energySupplied;
	}
	public class ConsumerListEntry{
		public Consumer consumer;
		public float energyToConsumer;
		public ConsumerListEntry(Consumer consumer, float energyToConsumer) {
			this.consumer= consumer;
			this.energyToConsumer = energyToConsumer;
		}
	}
	@Override
	public String toString() {
		return getModel().getName();
	}
}
