package ui.view;

import javax.swing.*;

import utility.ImageImport;

import java.awt.*;

/**
 * AboutUs Pop Up Window.
 *
 * @author Gruppe14
 */
public class AboutUsPopUp extends JFrame {
    /**
     * Serial.
     */
    private static final long serialVersionUID = 1L;
    private final JPanel contentPanel = new JPanel();
    private final JPanel contentPanel2 = new JPanel();
    private final JPanel contentPanel3 = new JPanel();
    private JLabel titel = new JLabel("HOLEG Simulator");
    private JLabel h1 = new JLabel("Project Management & Architect");
    private JLabel h2 = new JLabel("Software Developers");
    private JLabel h3 = new JLabel("Documentation");
    private JLabel h4 = new JLabel("Additional Collaborators");
    private JLabel description = new JLabel("A discrete-time simulator for modeling Smart Grids that follow a Holon-based model.");
    private JLabel namesManagement = new JLabel("C. Garcia Cordero");
    private JLabel namesDevelopersUntilV2 = new JLabel("K. Trometer, D. Rieder, T. Zheng, J. Widhalm, E. Palza, I. Dix");
    private JLabel namesDevelopersV2_1= new JLabel("A.T. Meyer-Berg, A. Schneider, T. Troppmann and L. Tietze");
    private JLabel namesDocumentation = new JLabel("E. Palza, C. Garcia Cordero");
    private JLabel namesCollaborators = new JLabel("R. Egert and F. Volk");
    private JLabel credits = new JLabel();

    /**
     * Constructor
     */
    public AboutUsPopUp(JFrame parentFrame) {
        super("About Us");

        // Set fonts
        Font fontTitle = new Font("Titel", 2, 35);
        titel.setFont(fontTitle);
        titel.setBounds(150, 30, 100, 20);
        Font headerFont = new Font("Header", 2, 20);
        h1.setFont(headerFont);
        h2.setFont(headerFont);
        h3.setFont(headerFont);
        h4.setFont(headerFont);

        // Set labels
        titel.setHorizontalAlignment(JLabel.CENTER);

        // Set layout
        contentPanel.setLayout(new BoxLayout(contentPanel, BoxLayout.Y_AXIS));
        contentPanel2.setLayout(new BoxLayout(contentPanel2, BoxLayout.Y_AXIS));
        contentPanel3.setLayout(new BoxLayout(contentPanel3, BoxLayout.Y_AXIS));
        
        this.setIconImage(ImageImport.loadImage("/Images/Holeg.png",30,30));
        setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
        setBounds(100, 100, 500, 800);
        setLocationRelativeTo(parentFrame);

        contentPanel.add(Box.createRigidArea(new Dimension(0, 15)));
        contentPanel.add(titel);
        contentPanel.add(Box.createRigidArea(new Dimension(0, 15)));
        contentPanel.add(description);
        contentPanel.add(Box.createRigidArea(new Dimension(0, 60)));
        getContentPane().add(contentPanel, BorderLayout.NORTH);

        contentPanel2.add(h1);
        contentPanel2.add(namesManagement);
        contentPanel2.add(Box.createRigidArea(new Dimension(0, 50)));
        contentPanel2.add(h2);
        contentPanel2.add(namesDevelopersUntilV2);
        contentPanel2.add(namesDevelopersV2_1);
        contentPanel2.add(Box.createRigidArea(new Dimension(0, 50)));
        contentPanel2.add(h3);
        contentPanel2.add(namesDocumentation);
        contentPanel2.add(Box.createRigidArea(new Dimension(0, 50)));
        contentPanel2.add(h4);
        contentPanel2.add(namesCollaborators);
        contentPanel2.add(Box.createRigidArea(new Dimension(0, 50)));
        contentPanel2.add(credits);
        getContentPane().add(contentPanel2, BorderLayout.CENTER);

        contentPanel3.add(Box.createRigidArea(new Dimension(0, 50)));
        //contentPanel3.add(namesDevelopers);
        getContentPane().add(contentPanel3, BorderLayout.SOUTH);
    }
}