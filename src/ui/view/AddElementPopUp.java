package ui.view;

import classes.AbstractCanvasObject;
import classes.HolonElement;
import classes.HolonObject;
import utility.ImageImport;

import javax.swing.*;
import javax.swing.border.EmptyBorder;

import java.awt.*;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;

/**
 * popup for adding an Holon Element to a holon Object.
 * 
 * @author Gruppe14
 * @author improved by A.T.M.-B. (Gruppe007)
 */
public class AddElementPopUp extends JDialog {

	/**
	 * Serial.
	 */
	private static final long serialVersionUID = 1L;
	/* Data */
	/** Holon Object the Element should be added to */
	private AbstractCanvasObject tempCps;
	/** Holon Element that should be edited (if in edit Modus */
	private HolonElement hl;
	
	/* GUI */
	/** Panel containing everything */
	private final JPanel contentPanel = new JPanel();
	/** Textfield for entering a Name for the Element */
	private JTextField elementName;
	/** Textfield for the energy the Element consumes/produces */
	private JTextField providedEnergy;
	/** Element is active if checked */
	JCheckBox checkBoxActive;
	/** Textfield to enter the amount of this Element */
	private JTextField amount;
	/** Textfield to enter the flexible Energy of the Element */
	private JTextField flexibleEnergy;
	/** Flexible if checkbox is checked */
	JCheckBox checkboxFlexible;

	/**
	 * Create the AddElementPopup Dialog
	 * @param parentFrame
	 */
	AddElementPopUp(JFrame parentFrame) {
		super((java.awt.Frame) null, true);
		this.setIconImage(ImageImport.loadImage("/Images/Holeg.png", 30, 30));
		setModalityType(java.awt.Dialog.ModalityType.APPLICATION_MODAL);
		setBounds(100, 100, 400, 245);
		setLocationRelativeTo(parentFrame);
		getContentPane().setLayout(new BorderLayout());
		contentPanel.setBorder(new EmptyBorder(5, 5, 5, 5));
		getContentPane().add(contentPanel, BorderLayout.CENTER);
		contentPanel.setLayout(null);
		this.setTitle("Add Element to Object");

		
		/* Element Name Textfield and Label */
		elementName = new JTextField();
		elementName.addKeyListener(new KeyListener() {
			@Override
			public void keyPressed(KeyEvent arg0) {
			}

			@Override
			public void keyReleased(KeyEvent e) {
			}

			@Override
			public void keyTyped(KeyEvent e) {
				elementName.setBackground(Color.WHITE);
			}
		});
		
		JLabel lblElementName = new JLabel("Element Name:");
		lblElementName.setBounds(10, 10, 100, 20);
		contentPanel.add(lblElementName);
		elementName.setBounds(130, 10, 110, 20);
		contentPanel.add(elementName);
		elementName.setColumns(10);
		
		/* Add Provided Energy Label and Textfield */
		JLabel lblProvidedEnergy = new JLabel("Provided Energy:");
		lblProvidedEnergy.setBounds(10, 50, 120, 20);
		contentPanel.add(lblProvidedEnergy);
		
		providedEnergy = new JTextField();
		providedEnergy.setBounds(130, 50, 110, 20);
		contentPanel.add(providedEnergy);
		providedEnergy.setColumns(10);
		providedEnergy.setText("0");
		
		checkBoxActive = new JCheckBox("Active");
		checkBoxActive.setSelected(true);
		checkBoxActive.setBounds(250, 50, 115, 20);
		contentPanel.add(checkBoxActive);
		
		/* Add Flexible Energy Textfield and CheckBox */
		JLabel lblFlexibleEnergy = new JLabel("Flexible Energy:");
		lblFlexibleEnergy.setBounds(10, 90, 100, 20);
		contentPanel.add(lblFlexibleEnergy);
		
		flexibleEnergy = new JTextField();
		flexibleEnergy.setText("0");
		flexibleEnergy.setColumns(10);
		flexibleEnergy.setBounds(130, 90, 110, 20);
		contentPanel.add(flexibleEnergy);
		
		checkboxFlexible = new JCheckBox("Flexible");
		checkboxFlexible.setBounds(250, 90, 115, 20);
		contentPanel.add(checkboxFlexible);
		
		/* Add Amount Textfield and Checkbox */
		JLabel lblAmount = new JLabel("Amount:");
		lblAmount.setBounds(10, 130, 100, 14);
		contentPanel.add(lblAmount);

		amount = new JTextField();
		amount.setBounds(130, 130, 110, 20);
		contentPanel.add(amount);
		amount.setColumns(10);
		amount.setText("1");

		/* Add Buttons and Actions */
		JPanel buttonPane = new JPanel();
		buttonPane.setLayout(new FlowLayout(FlowLayout.RIGHT));
		getContentPane().add(buttonPane, BorderLayout.SOUTH);

		JButton okButton = new JButton("OK");
		okButton.addActionListener(arg0 -> okAction());
		okButton.setActionCommand("OK");
		buttonPane.add(okButton);
		getRootPane().setDefaultButton(okButton);

		JButton cancelButton = new JButton("Cancel");
		cancelButton.setActionCommand("Cancel");
		buttonPane.add(cancelButton);
		cancelButton.addActionListener(e -> dispose());
	}

	/**
	 * Sets the actual Cps.
	 * 
	 * @param cps
	 *            actual Cps
	 */
	void setActualCps(AbstractCanvasObject cps) {
		this.tempCps = cps;
	}

	/**
	 * Returns the created Element.
	 * 
	 * @return the Element
	 */
	public HolonElement getElement() {
		return hl;
	}

	/**
	 * setElement that should be edited
	 * @param holonElement
	 */
	public void setElement(HolonElement holonElement) {
		hl = holonElement;
		elementName.setText(hl.getEleName());
		amount.setText(""+hl.getAmount());
		providedEnergy.setText(""+hl.getEnergyPerElement());
		checkBoxActive.setSelected(hl.isActive());
		
	}
	
	/**
	 * Trys to create/edit the Element
	 */
	private void okAction() {
		boolean repeated = false;
		for (HolonElement e : ((HolonObject) tempCps).getElements()) {
			if (elementName.getText().equals(e.getEleName())&&(hl == null || hl.getId()!=e.getId())) {
				repeated = true;
				break;
			}
		}
		if (elementName.getText().length() != 0 && !repeated) {
			try {
				float energy = Float.parseFloat(providedEnergy.getText());
				int elementAmount = Integer.parseInt(amount.getText());
				if(hl == null){
					//hl = new HolonElement(elementName.getText(), elementAmount, energy);
					//should not happen
					System.err.println("Undefined Element in 'AddElementPopUp'-class.");
				} else {
					hl.setEleName(elementName.getText());
					hl.setAmount(elementAmount);
					hl.setEnergyPerElement(energy);
					hl.setActive(checkBoxActive.isSelected());
				}
				dispose();
			} catch (NumberFormatException e) {
				JOptionPane.showMessageDialog(new JFrame(), "Please enter numbers in the Fields amount and Energy");
			}
		} else {
			if (elementName.getText().length() == 0) {
				JLabel errorString = new JLabel("No name");
				errorString.setBounds(240, 8, 100, 20);
				contentPanel.add(errorString);
			} else if (repeated) {
				JLabel errorString = new JLabel("Name already given");
				errorString.setBounds(250, 8, 100, 20);
				contentPanel.add(errorString);
			}
			elementName.setBackground(new Color(255, 50, 50));
		}
	}
}
