package ui.view;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.FlowLayout;
import java.awt.Image;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.ArrayList;

import javax.swing.DefaultListModel;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JFileChooser;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextField;
import javax.swing.SwingConstants;
import javax.swing.border.EmptyBorder;
import javax.swing.filechooser.FileNameExtensionFilter;

import classes.AbstractCanvasObject;
import classes.HolonElement;
import classes.HolonObject;
import classes.Pair;
import ui.controller.Control;
import utility.ImageImport;

/**
 * Popup for adding a Holon Object to a Category.
 * 
 * @author Gruppe14
 */
public class AddObjectPopUp extends JDialog {

	private static final long serialVersionUID = 1L;
	private AddElementPopUp addElement;
	private JTextField objectName;
	private JTextField sourcePath;
	private ArrayList<HolonElement> hElements;
	private DefaultListModel<String> listModel;
	private JList<String> list;
	private String imagePath;
	//	private HolonObject theObject;
	private Control controller;
	private File selectedFile = null;
	private String filePath = " ";
	private String givenCategory;
	private JLabel lblImagePreview;
	private AbstractCanvasObject toEdit;
	private boolean editState;
	private boolean imageChanged = false;


	/**
	 * Create the dialog.
	 * 
	 * @param edit
	 *            true if edit
	 * @param obj
	 *            the object
	 * @param cat
	 *            the categorie
	 */
    AddObjectPopUp(boolean edit, AbstractCanvasObject obj, String cat, JFrame parentFrame) {

        toEdit = obj;
		editState = edit;
		this.setIconImage(ImageImport.loadImage("/Images/Holeg.png",30,30));
		setBounds(100, 100, 450, 342);
        setLocationRelativeTo(parentFrame);
        getContentPane().setLayout(new BorderLayout());
		JPanel contentPanel = new JPanel();
		contentPanel.setBorder(new EmptyBorder(5, 5, 5, 5));
		getContentPane().add(contentPanel, BorderLayout.CENTER);
		contentPanel.setLayout(null);
		hElements = new ArrayList<>();
		this.setTitle("Add Object Menu");
		{
			JLabel lblName = new JLabel("Name:");
			lblName.setHorizontalAlignment(SwingConstants.CENTER);
			lblName.setBounds(28, 21, 76, 14);
			contentPanel.add(lblName);
		}
		{
			objectName = new JTextField();
			objectName.addKeyListener(new KeyListener() {
				@Override
				public void keyPressed(KeyEvent arg0) {

				}

				@Override
				public void keyReleased(KeyEvent e) {

				}

				@Override
				public void keyTyped(KeyEvent e) {
					objectName.setBackground(Color.WHITE);
				}
			});
			if (edit) {
				objectName.setText(obj.getName());
			}
			objectName.setBounds(98, 18, 172, 20);
			contentPanel.add(objectName);
			objectName.setColumns(10);
		}
		{
			JButton btnBrowseImage = new JButton("Browse Image");
			btnBrowseImage.setBounds(10, 75, 134, 23);
			contentPanel.add(btnBrowseImage);
			btnBrowseImage.addMouseListener(new MouseAdapter() {
				public void mouseClicked(MouseEvent e) {
					fileChooser();
				}
			});

		}
		{
			lblImagePreview = new JLabel("");
			lblImagePreview.setBounds(295, 3, 50, 50);
			contentPanel.add(lblImagePreview);
		}
		{
			sourcePath = new JTextField();
			sourcePath.addKeyListener(new KeyListener() {
				@Override
				public void keyPressed(KeyEvent arg0) {
				}

				@Override
				public void keyReleased(KeyEvent e) {
				}

				@Override
				public void keyTyped(KeyEvent e) {
					sourcePath.setBackground(Color.WHITE);
				}
			});
			if (edit) {
				lblImagePreview.setIcon(new ImageIcon(ImageImport.loadImage(obj.getImage(), 50, 50)));
			}
			sourcePath.setBounds(148, 77, 271, 20);
			if(edit) {
				this.filePath = obj.getImage();
				sourcePath.setText(filePath);
			}
			contentPanel.add(sourcePath);
			
			sourcePath.setColumns(10);
		}
		{
			JButton btnAddDefaultElement = new JButton("Add Element");
			btnAddDefaultElement.addActionListener(actionEvent -> {
                addElement = new AddElementPopUp(parentFrame);
                addElement.setActualCps(toEdit);
				addElement.setVisible(true);
				HolonElement hl = addElement.getElement();
				hl.setSaving(new Pair<>(givenCategory, objectName.getText()));
				// if (hl != null) {
				// hl.setSav(givenCategory);
				// }
				// hl.setObj(objectName.getText());
				addElement(hl);
			});

			btnAddDefaultElement.setBounds(270, 144, 142, 23);
			contentPanel.add(btnAddDefaultElement);
		}
		{
			JScrollPane scrollPane = new JScrollPane();
			scrollPane.setBounds(10, 114, 236, 150);
			contentPanel.add(scrollPane);
			{

				listModel = new DefaultListModel<String>();

				/*
				 * HolonElement hel = new HolonElement("Test", 100, 5); String
				 * name = hel.getEleName(); for (int i = 0; i < 11; i++) {
				 * hel.setEleName(name + i); addElement(hel); }
				 */
				list = new JList<String>(listModel);
				scrollPane.setViewportView(list);
			}
		}
		if (edit) {
			for (HolonElement e : ((HolonObject) obj).getElements()) {
				addElement(e);
			}
		}
		{
			JButton btnNewButton = new JButton("Delete Element");
			btnNewButton.addActionListener(actionEvent -> {
				int selectedIndex = list.getSelectedIndex();
				if (selectedIndex != -1) {
					listModel.remove(selectedIndex);
					hElements.remove(selectedIndex);
				}
			});
			btnNewButton.setBounds(270, 182, 142, 27);
			contentPanel.add(btnNewButton);
		}

		{
			JPanel buttonPane = new JPanel();
			buttonPane.setLayout(new FlowLayout(FlowLayout.RIGHT));
			getContentPane().add(buttonPane, BorderLayout.SOUTH);
			{
				JButton okButton = new JButton("OK");
				okButton.addMouseListener(new MouseAdapter() {
					public void mouseClicked(MouseEvent e) {
//						Component frame = null;
						if (objectName.getText().length() > 0) {
							if (sourcePath.getText().equals(filePath)) {
								imagePath = filePath;
								if (imageChanged)
									copieFile();
								imageChanged = false;
								// theObject = new
								// HolonObject(objectName.getText());
								// theObject.setElements(hElements);
								// theObject.setImage(imagePath);// TODO Auto-generated catch block
								try {
									if (editState) {
										controller.delObjectCategory(givenCategory, toEdit.getName());
										hElements.forEach(ele -> ele
												.setSaving(new Pair<>(ele.getSaving().getKey(), objectName.getText())));
										controller.addObject(controller.searchCategory(givenCategory),
												objectName.getText(), hElements, imagePath);

									} else {
										controller.addObject(controller.searchCategory(givenCategory),
												objectName.getText(), hElements, imagePath);
									}
								} catch (Exception e2) {
								}

								// controller.addObjectCategory(controller.searchCategory(givenCategory),
								// theObject);
								//
								// System.out.println(theObject.getImage());

								dispose();
							} else {
								sourcePath.setBackground(new Color(255, 50, 50));
							}
						} else {
							objectName.setBackground(new Color(255, 50, 50));
							if (!sourcePath.getText().equals(filePath))
								sourcePath.setBackground(new Color(255, 50, 50));

						}
					}
				});
				okButton.setActionCommand("OK");
				buttonPane.add(okButton);
				getRootPane().setDefaultButton(okButton);
			}
			{
				JButton cancelButton = new JButton("Cancel");
				cancelButton.setActionCommand("Cancel");
				buttonPane.add(cancelButton);
				cancelButton.addActionListener(e -> dispose());
			}
		}
	}


	/**
	 * adds a Holon Element.
	 *
	 * @param hl
	 *            the HolonElement
	 */
	private void addElement(HolonElement hl) {
		hElements.add(hl);
		listModel.addElement(hl.getAmount() + "x: " + hl.getEleName() + " " + hl.getEnergyPerElement() + "U");
	}

	/**
	 * Choose the file.
	 */
	private void fileChooser() {
		JFileChooser fileChooser = new JFileChooser();
		FileNameExtensionFilter filter = new FileNameExtensionFilter("png, jpg or jpeg", "png", "jpg", "jpeg");
		fileChooser.setFileFilter(filter);
		int returnValue = fileChooser.showOpenDialog(null);
		if (returnValue == JFileChooser.APPROVE_OPTION) {
			selectedFile = fileChooser.getSelectedFile();
			filePath = selectedFile.getAbsolutePath();
			sourcePath.setText(filePath);
			ImageIcon icon = new ImageIcon(//TODO: ugly
					
					new ImageIcon(filePath).getImage().getScaledInstance(50, 50, Image.SCALE_SMOOTH));
			lblImagePreview.setIcon(icon);
			imageChanged = true;
		} else {
			System.out.println("Failed to Load");
		}

	}

	/**
	 * Copies the File.
	 */
	private void copieFile() {
		InputStream inStream;
		OutputStream outStream;
		try {
			File source = new File(filePath);
			File dest = new File(System.getProperty("user.home") + "/.config/HolonGUI/Images/");
			dest.mkdirs();
			dest = new File(dest, selectedFile.getName());
			imagePath = "" + dest;

			inStream = new FileInputStream(source);
			outStream = new FileOutputStream(dest);
			byte[] buffer = new byte[1024];

			int length;
			while ((length = inStream.read(buffer)) > 0) {
				outStream.write(buffer, 0, length);
			}

			inStream.close();
			outStream.close();
			System.out.println("File Copied..");
		} catch (IOException eex) {
			eex.printStackTrace();
		}
	}

//	/**
//	 * Edit the Information.
//	 *
//	 * @param obj
//	 *            the CpsObject
//	 */
//	public void editInformation(HolonObject obj) {
//		objectName.setText(obj.getName());
//	}

//	/**
//	 * Return the Object.
//	 *
//	 * @return the CpsObject
//	 */
//	public HolonObject getObject() {
//		return theObject;
//	}

	/**
	 * Sets the Controller.
	 * 
	 * @param controller
	 *            the controller
	 */
	public void setController(Control controller) {
		this.controller = controller;
	}

	/**
	 * Set the Category.
	 * 
	 * @param cat
	 *            the Category
	 */
	public void setCategory(String cat) {
		givenCategory = cat;
	}
}