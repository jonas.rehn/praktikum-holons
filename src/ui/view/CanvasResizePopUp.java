package ui.view;

import ui.controller.Control;
import ui.model.Model;
import utility.ImageImport;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class CanvasResizePopUp extends JDialog {
    private final JButton btnOk = new JButton("OK");
	private final JButton btnCancel = new JButton("Cancel");
	JTabbedPane tabbedPane;
	JTabbedPane tabbedPane2;
	Model model;
	Control controller;
	MyCanvas canvas;
    private JPanel mainPanel = new JPanel();
    private JTextField tFieldWidht = new JTextField();
    private JTextField tFieldHeight = new JTextField();
    private JLabel lblWidth = new JLabel("Width:");
    private JLabel lblHeight = new JLabel("Height:");
    private JPanel buttonPanel = new JPanel();

	public CanvasResizePopUp(Model model, Control controller, MyCanvas canvas, JTabbedPane tabbedPane,
                             JTabbedPane tabbedPane2, JFrame parentFrame) {
        super((java.awt.Frame) null, true);
		this.tabbedPane = tabbedPane;
		this.tabbedPane2 = tabbedPane2;
		this.model = model;
		this.controller = controller;
		this.canvas = canvas;

		// properties and stuff
		this.setIconImage(ImageImport.loadImage("/Images/Holeg.png",30,30));
		this.setTitle("Set the Size of the View");
		setBounds(200, 100, 200, 100);
        setLocationRelativeTo(parentFrame);

		// MainPanel
		tFieldWidht.setText("" + model.getCanvasX());
		tFieldHeight.setText("" + model.getCanvasY());
		mainPanel.add(lblWidth);
		mainPanel.add(tFieldHeight);
		mainPanel.add(lblHeight);
		mainPanel.add(tFieldWidht);
		mainPanel.setBackground(Color.WHITE);

		// Button Panel
		btnOk.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				controller.setCanvasX(Integer.parseInt(tFieldWidht.getText()));
				controller.setCanvasY(Integer.parseInt(tFieldHeight.getText()));
				canvas.setPreferredSize(new Dimension(model.getCanvasX(), model.getCanvasY()));
				for (int i = 4; i < tabbedPane.getTabCount(); i++) {
					if (tabbedPane.getComponentAt(i) != null) {
						GroupNodeCanvas unc = ((GroupNodeCanvas) ((JScrollPane) tabbedPane.getComponentAt(i))
								.getViewport().getComponent(0));
						unc.setPreferredSize(new Dimension(model.getCanvasX(), model.getCanvasY()));
						unc.repaint();
					}
				}
				if (tabbedPane2 != null && tabbedPane2.getSelectedIndex() >= 4) {
					GroupNodeCanvas unc = ((GroupNodeCanvas) ((JScrollPane) tabbedPane2.getSelectedComponent())
							.getViewport().getComponent(0));
					unc.setPreferredSize(new Dimension(model.getCanvasX(), model.getCanvasY()));
					unc.repaint();
				}
				canvas.repaint();
				dispose();
			}
		});
		btnCancel.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				dispose();
			}
		});
		buttonPanel.add(btnOk);
		buttonPanel.add(btnCancel);
		buttonPanel.setBackground(Color.WHITE);

		// Add to ContentPane
		getContentPane().add(mainPanel, BorderLayout.CENTER);
		getContentPane().add(buttonPanel, BorderLayout.SOUTH);
	}

}
