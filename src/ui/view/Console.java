package ui.view;

import java.awt.BorderLayout;

import javax.swing.Box;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.JToolBar;
import javax.swing.text.DefaultCaret;

import utility.ImageImport;
/**
 * Little new swing object to print data to a console.
 * @author tom
 *
 */
public class Console extends JPanel {
	private JTextArea textArea = new JTextArea();
	private JScrollPane scrollPane;
	
	public Console() {
		super();
		this.setLayout(new BorderLayout());
		textArea.setEditable(false);
		DefaultCaret caret = (DefaultCaret)textArea.getCaret();
		caret.setUpdatePolicy(DefaultCaret.ALWAYS_UPDATE);
		scrollPane = new JScrollPane(textArea);
		this.add(scrollPane, BorderLayout.CENTER);
		JToolBar toolBar = new JToolBar();
		toolBar.setFloatable(false);
		JButton clearButton  = new JButton("", new ImageIcon(ImageImport.loadImage("/Button_Images/Clear.png", 24, 24)));
		clearButton.setToolTipText("Clear Console");
		clearButton.addActionListener(actionEvent -> clear());
		toolBar.add(clearButton);
		toolBar.add(Box.createHorizontalGlue());
		JButton topButton = new JButton("", new ImageIcon(ImageImport.loadImage("/Button_Images/Top.png", 24, 24)));
		topButton.setToolTipText("Scroll to top");
		topButton.addActionListener(actionEvent -> scrollToTop());
		toolBar.add(topButton);
		JButton botButton = new JButton("", new ImageIcon(ImageImport.loadImage("/Button_Images/Bottom.png", 24, 24)));
		botButton.setToolTipText("Scroll to bottom");
		botButton.addActionListener(actionEvent -> scrollToBottom());
		toolBar.add(botButton);
		scrollPane.setColumnHeaderView(toolBar);
	}
	private void scrollToTop() {
		textArea.setCaretPosition(0);
	}
	private void scrollToBottom() {
		textArea.setCaretPosition(textArea.getDocument().getLength());
	}
	
	
	
	
	
	
	public void clear() {
		textArea.setText("");
	}
	public void print(String message) {
		textArea.append(message);
		
	}
	public void println(String message) {
		textArea.append(message  + "\n");
	}
}
