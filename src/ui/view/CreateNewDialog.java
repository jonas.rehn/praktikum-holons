package ui.view;

import java.awt.BorderLayout;
import java.awt.CardLayout;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.GridLayout;
import java.io.IOException;

import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JDialog;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;
import classes.HolonObject;
import ui.controller.Control;



public class CreateNewDialog extends JDialog{
	//DefaultConstructor
	String[] optionStrings = { "","Category", "Object", "Battery", "Switch"};
	public static enum Option {
		None, Category, Object, Switch;
		public static Option getEnumAtIndex(int desired){
			if(desired>=0 && desired<=4)
				return values()[desired];
			else 
				return None;
		}
		
	};
	Option actualOption = Option.None;
	Control actualController;
	
	//important JPanelItems
	JComboBox<String> optionList = new JComboBox<String>(optionStrings);
	JTextField inputName = new JTextField();
	JButton saveButton = new JButton("Save");
	//TODO
	JTextField inputNameSwitch = new JTextField();
	JComboBox<String> selectedCategorySwitch = new JComboBox<String>();
	
	
	
	CreateNewDialog(Control controller, JFrame parentFrame){
		super((JFrame)parentFrame, "Create a..");
		actualController = controller;
		setVisible(true);
		JPanel contentPanel = new JPanel();
		contentPanel.setLayout(new BorderLayout());
		contentPanel.add(makeTopPanel(), BorderLayout.PAGE_START);
		JPanel cards = new JPanel(new CardLayout());
		cards.add(makeNothingPanel(), Option.None.name());
		cards.add(makeCategoryPanel(), Option.Category.name());
		cards.add(makeObjectPanel(), Option.Object.name());
		cards.add(makeSwitchPanel(), Option.Switch.name());
		contentPanel.add(cards, BorderLayout.CENTER);
		
		optionList.addActionListener(actionEvent -> {
			CardLayout cl = (CardLayout)(cards.getLayout());
			actualOption = Option.getEnumAtIndex(optionList.getSelectedIndex());
		    cl.show(cards, actualOption.name());
		    saveButton.setEnabled(actualOption != Option.None);  
		    //TODO delete and make own iterface
		    saveButton.setVisible(actualOption != Option.Object);	
		});
		
		
        contentPanel.add(makeBottemPanel(), BorderLayout.PAGE_END);
        addSaveButtonLogik();
        add(contentPanel);
        saveButton.setEnabled(false);	
		setMinimumSize(new Dimension(400,50));
		pack();
		setLocationRelativeTo(parentFrame);
	}


	CreateNewDialog(Control controller, Option aOption, JFrame parentFrame){
		super((JFrame)parentFrame, "Create a " + aOption.name());
		actualController = controller;
		if(aOption == Option.None)
			dispose();
		setVisible(true);
		JPanel contentPanel = new JPanel();
		contentPanel.setLayout(new BorderLayout());
		JPanel content;
		switch(aOption)
		{
		case Category:
			content = makeCategoryPanel();
			break;
		case Object:
			content = makeObjectPanel();
			break;
		case Switch:
			content = makeSwitchPanel();
			break;
		default:
			content = makeNothingPanel();
			break;
		}
		actualOption = aOption;
		contentPanel.add(content, BorderLayout.CENTER);
		contentPanel.add(makeBottemPanel(), BorderLayout.PAGE_END);
		addSaveButtonLogik(); 
		//TODO delete and make own iterface
	    if(aOption == Option.Object) saveButton.setVisible(false);
		add(contentPanel);	
		setMinimumSize(new Dimension(400,50));
		pack();
		setLocationRelativeTo(parentFrame);
	}
	
	
	private JPanel makeTopPanel() {
		JPanel topPanel = new JPanel(new GridLayout(1,2));
		JLabel text = new JLabel("Choose..");
		topPanel.add(text);
		topPanel.add(optionList);
		return topPanel;
	}

	private JPanel makeBottemPanel() {
		JPanel bottomPanel = new JPanel();
        bottomPanel.setLayout(new FlowLayout(FlowLayout.RIGHT));
       
        bottomPanel.add(saveButton);
        JButton cancelButton = new JButton("Cancel");
        cancelButton.addActionListener(actionEvent -> dispose());
        bottomPanel.add(cancelButton);
		return bottomPanel;
	}

	private JPanel makeObjectPanel() {
		JPanel objectPanel = new JPanel();
		JLabel categoryText = new JLabel("In Category:");
		objectPanel.add(categoryText);
		JComboBox<String> selectedCategory = new JComboBox<String>(actualController.getCategoriesStrings());
		objectPanel.add(selectedCategory);
		
		
		JButton nextButton = new JButton("Next");
		objectPanel.add(nextButton);
		nextButton.addActionListener(actionEvent -> {
			makeOldObjectPopUp(selectedCategory.getSelectedItem().toString());
		});
		return objectPanel;
	}

	private void makeOldObjectPopUp(String categoryName) {
		//TODO: del and make own on
		
		HolonObject hO = new HolonObject("");
		
		AddObjectPopUp addObjectPopUP = new AddObjectPopUp(false,hO , "hei", null);
		addObjectPopUP.setVisible(true);
		addObjectPopUP.setController(actualController);
		addObjectPopUP.setCategory(categoryName);
		dispose();
	}


	private JPanel makeNothingPanel() {
		return new JPanel();
	}
	
	private JPanel makeCategoryPanel()
	{
		JPanel newCategory = new JPanel(new FlowLayout());
		JLabel categoryName = new JLabel("Name:");
		String initialText = "The name of the new category";
		inputName.setText(initialText);
		inputName.setBackground(java.awt.Color.LIGHT_GRAY);
		inputName.addFocusListener(new java.awt.event.FocusAdapter() {
		    public void focusGained(java.awt.event.FocusEvent evt) {
		       if (inputName.getText().equals(initialText)) {
		    	   inputName.setText("");
		    	   inputName.setBackground(java.awt.Color.WHITE);
		       }
		    }
		    public void focusLost (java.awt.event.FocusEvent evt) {
			       if (inputName.getText().equals("")) {
			    	   inputName.setText(initialText);
			    	   inputName.setBackground(java.awt.Color.LIGHT_GRAY);
			       }
			    }
		});
		inputName.setColumns(20);
		newCategory.add(categoryName);
		newCategory.add(inputName);
		return newCategory;
	}
	private JPanel makeSwitchPanel()
	{
		JPanel objectPanel = new JPanel();
		JLabel categoryText = new JLabel("In Category:");
		objectPanel.add(categoryText);
		selectedCategorySwitch = new JComboBox<String>(actualController.getCategoriesStrings());
		objectPanel.add(selectedCategorySwitch);
		
		
		JLabel switchName = new JLabel("Name:");
		String initialText = "The name of the new switch";
		
		inputNameSwitch.setText(initialText);
		inputNameSwitch.setBackground(java.awt.Color.LIGHT_GRAY);
		inputNameSwitch.addFocusListener(new java.awt.event.FocusAdapter() {
		    public void focusGained(java.awt.event.FocusEvent evt) {
		       if (inputNameSwitch.getText().equals(initialText)) {
		    	   inputNameSwitch.setText("");
		    	   inputNameSwitch.setBackground(java.awt.Color.WHITE);
		       }
		    }
		    public void focusLost (java.awt.event.FocusEvent evt) {
			       if (inputNameSwitch.getText().equals("")) {
			    	   inputNameSwitch.setText(initialText);
			    	   inputNameSwitch.setBackground(java.awt.Color.LIGHT_GRAY);
			       }
			    }
		});
		inputNameSwitch.setColumns(20);
		objectPanel.add(switchName);
		objectPanel.add(inputNameSwitch);
		return objectPanel;
	}	
	private void addSaveButtonLogik() {
		saveButton.addActionListener(actionEvent -> {
			if(actualOption == Option.Category)
			{
				try {
					actualController.addCategory(inputName.getText());
				} catch (IOException e) {
					System.err.println("IOException addCategory");
				}
				dispose();
			}
			else if (actualOption == Option.Switch)
			{//TODO
				try {
					actualController.addSwitch(actualController.searchCategory(selectedCategorySwitch.getSelectedItem().toString()), inputNameSwitch.getText());
				} catch (IOException e) {
					System.err.println("IOException addSwitch");
				}
				dispose();
			}
		});
	}
}
