package ui.view;

import javax.swing.table.DefaultTableModel;

/**
 * Default Table.
 * 
 * @author Gruppe14
 */
public class DefaulTable extends DefaultTableModel {

	private static final long serialVersionUID = 1L;
	private boolean[][] editableCells; // 2d array to represent rows and
										// columns

	/**
	 * Constructor.
	 * 
	 * @param rows
	 *            the Rows
	 * @param cols
	 *            the Cols
	 */
	public DefaulTable(int rows, int cols) { // constructor
		super(rows, cols);
		this.editableCells = new boolean[rows][cols];
	}

	@Override
	public boolean isCellEditable(int row, int column) {
		return this.editableCells[row][column];
	}

	/**
	 * Set Cell Editable.
	 * 
	 * @param row
	 *            the Rows
	 * @param col
	 *            the Cols
	 * @param value
	 *            true or false
	 */
	public void setCellEditable(int row, int col, boolean value) {
		this.editableCells[row][col] = value; // set cell true/false
		this.fireTableCellUpdated(row, col);
	}

}