package ui.view;

import java.awt.BorderLayout;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.ButtonGroup;
import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JRadioButton;
import javax.swing.JTextField;
import javax.swing.border.EmptyBorder;

import classes.Edge;
import ui.controller.Control;

/**
 * Popup for Editing Edges.
 * 
 * @author Gruppe14
 */
public class EditEdgesPopUp extends JDialog {

	private static final long serialVersionUID = 1L;
	private final JPanel contentPanel = new JPanel();
	private JTextField capacityField;
	private float capacity;
	private JRadioButton rdbtnChangeForAll;
	private JRadioButton rdbtnChangeForNew;
	private JRadioButton rdbtnChangeForAll1;
	private Control controller;
	private MyCanvas canvas;

	/**
	 * Launch the application.
	 * 
	 * @param args
	 *            standard
	 */


	/**
	 * Constructor.
	 */
    public EditEdgesPopUp(JFrame parentFrame) {
        super((java.awt.Frame) null, true);
		setModalityType(java.awt.Dialog.ModalityType.APPLICATION_MODAL);
		this.setTitle("Edit Capacities of Edges");
		setBounds(100, 100, 400, 220);
        setLocationRelativeTo(parentFrame);
        getContentPane().setLayout(new BorderLayout());
		contentPanel.setBorder(new EmptyBorder(5, 5, 5, 5));
		getContentPane().add(contentPanel, BorderLayout.CENTER);
		contentPanel.setLayout(null);

		JLabel lblMaximumCapacity = new JLabel("Maximum Capacity:");
		lblMaximumCapacity.setFont(new Font("Tahoma", Font.PLAIN, 11));
		lblMaximumCapacity.setBounds(10, 11, 98, 14);
		contentPanel.add(lblMaximumCapacity);

		capacityField = new JTextField();
		capacityField.setBounds(107, 8, 120, 20);
		contentPanel.add(capacityField);
		capacityField.setColumns(10);

		rdbtnChangeForAll = new JRadioButton("Change for all existing Edges only");
		rdbtnChangeForAll.setBounds(10, 39, 330, 23);
		contentPanel.add(rdbtnChangeForAll);

		rdbtnChangeForNew = new JRadioButton("Change for new created Edges only");
		rdbtnChangeForNew.setBounds(10, 65, 330, 23);
		contentPanel.add(rdbtnChangeForNew);

		rdbtnChangeForAll1 = new JRadioButton("Change for all existing and new created Edges");
		rdbtnChangeForAll1.setBounds(10, 95, 400, 23);
		contentPanel.add(rdbtnChangeForAll1);

		JButton btnCancel = new JButton("Cancel");
		btnCancel.setActionCommand("Cancel");
		btnCancel.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				dispose();
			}
		});
		btnCancel.setBounds(285, 147, 89, 23);
		contentPanel.add(btnCancel);

		JButton btnOk1 = new JButton("OK");
		btnOk1.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
					try {
						capacity = Float.parseFloat(capacityField.getText().toString());
						if (capacity < 0) {
							throw new NumberFormatException();
						}
						if (rdbtnChangeForAll.isSelected()) {
							changeForExisting(capacity);
							dispose();
						} else if (rdbtnChangeForNew.isSelected()) {
							changeForNew(capacity);
							dispose();
						} else if (rdbtnChangeForAll1.isSelected()) {
							changeForExAndNew(capacity);
							dispose();
						} else {
							JOptionPane.showMessageDialog(new JFrame(), "Please select one of the options");
						}
					} catch (NumberFormatException eex) {
						JOptionPane.showMessageDialog(new JFrame(), "Please enter a number greater or equal 0 in the Field for Maximum Capacity");
					}
			}
		});
		btnOk1.setBounds(186, 147, 89, 23);
		contentPanel.add(btnOk1);
		this.setTitle("Edit Edge Capacities");
		ButtonGroup bG = new ButtonGroup();
		bG.add(rdbtnChangeForAll1);
		bG.add(rdbtnChangeForNew);
		bG.add(rdbtnChangeForAll);
	}

	/**
	 * Set the Canvas.
	 * 
	 * @param can
	 *            the Canvas
	 */
	public void setCanvas(MyCanvas can) {
		canvas = can;
	}

	/**
	 * set the Controller.
	 * 
	 * @param cont
	 *            the Controller
	 */
	public void setController(Control cont) {
		controller = cont;
	}

	/**
	 * set edge capacity for new edges.
	 * 
	 * @param cap
	 *            the capacity
	 */
	public void changeForNew(float cap) {
		controller.setMaxCapacity(cap);
		controller.resetSimulation();
	}

	/**
	 * Set Capacity for all existing Edges.
	 * 
	 * @param cap
	 *            the Capacity
	 */
	public void changeForExisting(float cap) {
		/*
		 * for(SubNet sn: controller.getSimManager().getSubNets()){ for(CpsEdge
		 * edge: sn.getEdges()){ edge.setCapacity(cap); } } for(CpsEdge edge:
		 * controller.getSimManager().getBrokenEdges()){ edge.setCapacity(cap);
		 * }
		 */
		for (Edge edge : controller.getModel().getEdgesOnCanvas()) {
			edge.setCapacity(cap);
        }
		controller.resetSimulation();
		controller.calculateStateAndVisualForCurrentTimeStep();
		canvas.repaint();
	}


	/**
	 * Set the Capacity for all existing and new edges.
	 * 
	 * @param cap
	 *            the capacity
	 */
	public void changeForExAndNew(float cap) {
		changeForNew(cap);
		changeForExisting(cap);
	}

}
