package ui.view;

import interfaces.LocalMode;
import ui.model.Model;


public class IndexTranslator {
	public static int STANDARD_GRAPH_ACCURACY = 100;
	// TODO assign model
	// this refrence should be updated with the model
	public static Model model;
	   /**
     * Determines the index of the internal value array
     * of an element that should be used, since elements only save 100 values,
     * but iterations and local period can be anything between 1 and 100000.
     * 
     * @param m the corresponding model.
     * @param e the element for which the calculation should be made.
     * @param timeStep the iteration for which the calculation should be made.
     * @return
     */
    public static int getEffectiveIndex(LocalMode e, int timeStep){
    	if(e.isUsingLocalPeriod()) {
    		return timeStep % e.getLocalPeriod()* 100 /e.getLocalPeriod();
    	}
    	else {
    		return timeStep * 100 / (model == null?STANDARD_GRAPH_ACCURACY:model.getIterations());
    	}
    }
}
