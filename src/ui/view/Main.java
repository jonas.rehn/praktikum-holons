package ui.view;

import ui.controller.Control;
import ui.model.Model;

import javax.swing.*;

import java.awt.*;

/**
 * The main Class in this Program. The GUI is created in this Class.
 * 
 * @author Gruppe14
 * 
 */
public class Main {

	/**
	 * main method of this program.
	 * 
	 * @param args
	 *            standard
	 */
	public static void main(String[] args) {
		if (!System.getProperty("os.name").startsWith("Linux")) {
			loadNotLinuxLookAndFeel();	
		}
		
        EventQueue.invokeLater(() -> {
            try {
                Model model = new Model();
                Control control = new Control(model);
                GUI view = new GUI(control);
                IndexTranslator.model = model;
                view.getFrmCyberPhysical().setVisible(true);

            } catch (Exception e) {
                e.printStackTrace();
            }
        });
    }

	private static void loadNotLinuxLookAndFeel() {
		try {
		UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName());
		} catch (ClassNotFoundException | InstantiationException | IllegalAccessException | UnsupportedLookAndFeelException e) {
		e.printStackTrace();
		}
	}

}
