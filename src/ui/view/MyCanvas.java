package ui.view;

import java.awt.BasicStroke;
import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.RenderingHints;
import java.awt.datatransfer.UnsupportedFlavorException;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.event.MouseMotionListener;
import java.awt.geom.Line2D;
import java.awt.image.BufferedImage;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashSet;

import javax.swing.ImageIcon;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JScrollPane;
import javax.swing.JTabbedPane;
import javax.swing.SwingUtilities;

import com.google.gson.JsonParseException;

import classes.AbstractCanvasObject;
import classes.Edge;
import classes.GroupNode;
import classes.HolonObject;
import classes.HolonSwitch;
import classes.Node;
import classes.Position;
import ui.controller.Control;
import ui.controller.UpdateController;
import ui.model.Consumer;
import ui.model.DecoratedCable;
import ui.model.DecoratedGroupNode;
import ui.model.DecoratedHolonObject;
import ui.model.DecoratedHolonObject.HolonObjectState;
import ui.model.DecoratedSwitch;
import ui.model.ExitCable;
import ui.model.DecoratedSwitch.SwitchState;
import ui.model.Model;
import ui.model.Passiv;
import ui.model.Supplier;
import ui.model.VisualRepresentationalState;
import utility.ImageImport;

/**
 * This Class is the Canvas. All Objects will be visualized here
 *
 * @author Gruppe14
 */
public class MyCanvas extends AbstractCanvas implements MouseListener,
		MouseMotionListener {

	private static final long serialVersionUID = 1L;
	public boolean disabled = false;
	/**
	 * Constructor.
	 *
	 * @param mod
	 *            the Model
	 * @param control
	 *            the Controller
	 * @param unitGraph
	 */
	public MyCanvas(Model mod, Control control, UnitGraph unitGraph) {
		toolTip = false;
		this.controller = control;
		this.model = mod;
		
		scalediv20 = model.getScale() / 20;
		
		showConnectionInformation = true;
		showedInformation[1] = true;
		showedInformation[3] = false;
		showedInformation[4] = true;
		control.setMaxCapacity(10000);
		popmenu.add(itemCut);
		popmenu.add(itemCopy);
		popmenu.add(itemPaste);
		popmenu.add(itemDelete);
		popmenu.add(itemAlign);
		popmenu.addSeparator();
		popmenu.add(itemGroup);
		popmenu.add(itemUngroup);
		popmenu.add(itemCreateTemplate);

		updCon = new UpdateController(mod, control);

		itemDelete.setEnabled(false);
		itemCut.setEnabled(false);
		itemCopy.setEnabled(false);
		itemPaste.setEnabled(true);
		itemAlign.setEnabled(false);
		itemGroup.setEnabled(false);
		itemUngroup.setEnabled(false);

		itemCut.setText("Cut");
		
		itemGroup.addActionListener(actionEvent -> {
			// calculate uppernode pos (taken from the controller)
				unPos = new Position(0, 0);
				animCps = new ArrayList<>();
				for (AbstractCanvasObject cps : model.getSelectedCpsObjects()) {
					animCps.add(cps); // add to animation Cps ArrayList
					unPos.x += cps.getPosition().x;
					unPos.y += cps.getPosition().y;
				}
				unPos.x /= animCps.size();
				unPos.y /= animCps.size();

				// save old Position
				savePos = new ArrayList<>();
				for (int i = 0; i < animCps.size(); i++) {
					savePos.add(new Position(0, 0));
					savePos.get(i).x = animCps.get(i).getPosition().x;
					savePos.get(i).y = animCps.get(i).getPosition().y;
				}

				animT = new javax.swing.Timer(animDelay, actionEvent1 -> {
					if (animDuration - animDelay > 0 && animCps.size() > 1) {
						for (AbstractCanvasObject animCpObject : animCps) {
							double x1 = animCpObject.getPosition().x - unPos.x;
							double y1 = animCpObject.getPosition().y - unPos.y;
							animCpObject.getPosition().x -= x1 / animSteps;
							animCpObject.getPosition().y -= y1 / animSteps;
						}
						repaint();
						animDuration -= animDelay;
						animSteps--;
					} else {
						animDuration = ANIMTIME;
						animSteps = animDuration / animDelay;
						animT.stop();
						for (int i = 0; i < animCps.size(); i++) {
							animCps.get(i).getPosition().x = savePos.get(i).x;
							animCps.get(i).getPosition().y = savePos.get(i).y;
						}
						controller.addUpperNode("GroupNode", null, animCps);
						controller.calculateStateAndVisualForCurrentTimeStep();
						triggerUpdateController();
						model.getSelectedCpsObjects().clear();
						repaint();
					}
				});
				animT.start();
			});

		itemUngroup
				.addActionListener(actionEvent -> {
					// save old Position
					int upperNodeId = tempCps.getId();
					
					closeUpperNodeTab(upperNodeId);

					savePos = new ArrayList<>();
					animCps = ((GroupNode) tempCps).getNodes();
					controller.ungroupGroupNode((GroupNode) tempCps, null);

					for (int i = 0; i < animCps.size(); i++) {
						savePos.add(new Position(0, 0));
						savePos.get(i).x = animCps.get(i).getPosition().x;
						savePos.get(i).y = animCps.get(i).getPosition().y;
					}
					for (AbstractCanvasObject cps : animCps) {
						int x = tempCps.getPosition().x;
						int y = tempCps.getPosition().y;

						cps.setPosition(new Position(x, y));
					}

					animT = new javax.swing.Timer(
							animDelay,
							actionEvent1 -> {
								model.getSelectedCpsObjects().clear();
								if (animDuration - animDelay >= 0) {
									for (int i = 0; i < animCps.size(); i++) {
										Position pos = animCps.get(i).getPosition();
										double x1 = pos.x - savePos.get(i).x;
										double y1 = pos.y - savePos.get(i).y;
										animCps.get(i).getPosition().x -= x1 / animSteps;
										animCps.get(i).getPosition().y -= y1 / animSteps;
									}
									repaint();
									animDuration -= animDelay;
									animSteps--;
								} else {
									animDuration = ANIMTIME;
									animSteps = animDuration / animDelay;
									animT.stop();
									for (int i = 0; i < animCps.size(); i++) {
										animCps.get(i).getPosition().x = savePos
												.get(i).x;
										animCps.get(i).getPosition().y = savePos
												.get(i).y;
									}
									controller.calculateStateAndVisualForCurrentTimeStep();
									triggerUpdateController();
									repaint();
								}
							});
					animT.start();
				});

		// adds the selected object(s) to the statistic panel

		itemDelete.addActionListener(actionEvent -> {
			// Remove the selected Object objects
				//Edge Deleting
				if (tempCps == null && edgeHighlight != null) {
					controller.removeEdgesOnCanvas(edgeHighlight);
					//Look for a CPSNode with no Connections and delete them
					if(edgeHighlight.getA().getClass() == Node.class && edgeHighlight.getA().getConnections().size() == 0){ 
						controller.delCanvasObject(edgeHighlight.getA(), false);	
					}
					if(edgeHighlight.getB().getClass() == Node.class && edgeHighlight.getB().getConnections().size() == 0){ //Look on the other end of the cable
						controller.delCanvasObject(edgeHighlight.getB(), false);
					}
					edgeHighlight = null;
				}
				boolean save = false;
				for (int j = 0; j < model.getSelectedCpsObjects().size(); j++) {
					AbstractCanvasObject cps = model.getSelectedCpsObjects()
							.get(j);
					if (j == model.getSelectedCpsObjects().size() - 1)
						save = true;
					controller.delCanvasObject(cps, save);
					// Remove UpperNodeTab if UpperNode deleted
				if (cps instanceof GroupNode) {
					JTabbedPane tabbedPane = (JTabbedPane)getParent().getParent()
							.getParent().getParent();
					// Look if the uppernode is open in a Tab
					for (int i = 4; i < tabbedPane.getTabCount(); i++) {
						if (tabbedPane.getComponentAt(i) != null
								&& ((GroupNodeCanvas) ((JScrollPane) tabbedPane
										.getComponentAt(i)).getViewport()
										.getComponent(0)).upperNode.getId() == cps
										.getId()) {
							((ButtonTabComponent) tabbedPane
									.getTabComponentAt(i)).removeTabs();
							break;
						}
					}
				}
				toolTip = false;
			}
			model.getSelectedCpsObjects().clear();
			tempCps = null;
			repaint();
		});

		itemCut.addActionListener(actionEvent -> {
			controller.cut(null);
			itemPaste.setEnabled(true);
			repaint();
		});

		itemCopy.addActionListener(actionEvent -> {
			if(tempCps instanceof GroupNode)
				controller.getObjectsInDepth();
			controller.copy(null);
			itemPaste.setEnabled(true);
			repaint();
		});

		itemAlign.addActionListener(actionEvent ->
		{
			for(AbstractCanvasObject cps: model.getSelectedCpsObjects())
				align(cps, model.getScaleDiv2());
			repaint();
		});
		itemPaste
				.addActionListener(actionEvent -> {
					try {
						controller.paste(null, mousePosition);

					} catch (JsonParseException | UnsupportedFlavorException
							| IOException e1) {
						JLabel message = new JLabel(
								"The Clipboard information cannot be pastet into Application.");
						JOptionPane.showMessageDialog(null, message, "",
								JOptionPane.ERROR_MESSAGE);
					}
					repaint();
				});
		
		/*
		 * create Template
		 */
		itemCreateTemplate.addActionListener(actionEvent -> {
			controller.createTemplate((HolonObject)tempCps,(JFrame)SwingUtilities.getRoot(this));
		});

		this.addMouseListener(this);
		this.addMouseMotionListener(this);
	}

	/**
	 * Paints all Components on the Canvas.
	 *
	 * @param g
	 *            Graphics
	 */
	
	private Color getStateColor(HolonObjectState state) {
		switch(state) {
		case NOT_SUPPLIED:
			return new Color(230, 120, 100);
		case NO_ENERGY:
			return Color.white;
		case OVER_SUPPLIED:
			return new Color(166, 78, 229);
		case PARTIALLY_SUPPLIED:
			return Color.yellow;
		case PRODUCER:
			return Color.lightGray;
		case SUPPLIED:
			return new Color(13, 175, 28);
		default:
			return Color.BLACK;
		}
	}
	private void paintCanvasObject(Graphics2D g, DecoratedHolonObject decoratedHolonObject){
		Position pos = decoratedHolonObject.getModel().getPosition();
		Color statecolor = getStateColor(decoratedHolonObject.getState());
		g.setColor(statecolor);
		g.fillRect(pos.x - controller.getScaleDiv2(), pos.y - controller.getScaleDiv2(), controller.getScale(), controller.getScale());
		drawCanvasObject(g, decoratedHolonObject.getModel().getImage(), pos);
	}
	private void drawCanvasObjectString(Graphics2D g, Position posOfCanvasObject, float energy) {
		g.setColor(Color.BLACK);
		g.setFont(new Font("TimesNewRoman", Font.PLAIN, (int) (controller.getScale() / 4f) )); 
		g.drawString((energy > 0)? "+" + Float.toString(energy): Float.toString(energy), posOfCanvasObject.x - controller.getScaleDiv2(), posOfCanvasObject.y - controller.getScaleDiv2() - 1);
	}
	private void paintConsumer(Graphics2D g, Consumer con){
		paintCanvasObject(g, con);
		paintSupplyBar(g,con.getSupplyBarPercentage(), getStateColor(con.getState()), con.getModel().getPosition());
		drawCanvasObjectString(g, con.getModel().getPosition(), -con.getEnergyNeededFromNetwork());
	}
	private void paintSupplier(Graphics2D g, Supplier sup){
		paintCanvasObject(g, sup);
		drawCanvasObjectString(g, sup.getModel().getPosition(), sup.getEnergyToSupplyNetwork());
	}
	
	private void drawCanvasObject(Graphics2D g, String Image, Position pos) {
		g.drawImage(ImageImport.loadImage(Image, controller.getScale(), controller.getScale()) , 
				pos.x - controller.getScaleDiv2(),
				pos.y - controller.getScaleDiv2(),
				controller.getScale(), controller.getScale() , null);
	}
	
	private void paintCable(Graphics2D g, DecoratedCable cable, boolean isSelected)
	{
		Position start = cable.getModel().getA().getPosition();
		Position end =  cable.getModel().getB().getPosition();
		float currentEnergy = cable.getFlowEnergy();
		float capacity = cable.getModel().getCapacity();
		boolean unlimited = cable.getModel().isUnlimitedCapacity();
		switch(cable.getState()) {
		case Burned:
			g.setColor(Color.RED);
			g.setStroke(new BasicStroke(2));
			break;
		case Working:
			g.setColor(new Color(13, 175, 28));
			g.setStroke(new BasicStroke(unlimited?2f:(currentEnergy / capacity * 2f) + 1));
			break;
		}
		if(isSelected){
			g.setColor(Color.lightGray);
		}
		g.drawLine(start.x, start.y, end.x, end.y);
		if(showConnectionInformation) {			
			Position middle = new Position((start.x + end.x) / 2, (start.y + end.y) / 2);
			g.setFont(new Font("TimesRoman", Font.PLAIN, Math.max((int) (controller.getScale() / 3.5f), 10) )); 
			g.drawString(currentEnergy + "/" + (unlimited?"\u221E":capacity) , middle.x, middle.y);
		}
	}
	private void paintSwitch(Graphics2D g, DecoratedSwitch dSwitch)
	{
		drawCanvasObject(g, dSwitch.getState() == SwitchState.Open ? HolonSwitch.getSwitchOpenImage(): HolonSwitch.getSwitchClosedImage() , dSwitch.getModel().getPosition());
	}
	
	private void paintExitCable(Graphics2D g, ExitCable eCable) {
		Position start = eCable.getStart().getPosition();
		Position end = eCable.getFinish().getPosition();
		float currentEnergy = eCable.getCable().getFlowEnergy();
		float capacity = eCable.getCable().getModel().getCapacity();
		boolean unlimited = eCable.getCable().getModel().isUnlimitedCapacity();
		switch(eCable.getCable().getState()) {
		case Burned:
			g.setColor(Color.RED);
			g.setStroke(new BasicStroke(2));
			break;
		case Working:
			g.setColor(new Color(13, 175, 28));
			g.setStroke(new BasicStroke(unlimited?2f:(currentEnergy / capacity * 2f) + 1));
			break;
		}
		g.drawLine(start.x, start.y, end.x, end.y);
		Position middle = new Position((start.x + end.x) / 2, (start.y + end.y) / 2);
		g.setFont(new Font("TimesRoman", Font.PLAIN, Math.max((int) (controller.getScale() / 3.5f), 10) )); 
		g.drawString(currentEnergy + "/" + (unlimited?"\u221E":capacity) , middle.x, middle.y);
	}
	private void paintGroupNode(Graphics2D g, DecoratedGroupNode dGroupNode) {
		Position pos = dGroupNode.getModel().getPosition();
		g.setColor(Color.lightGray);
		g.fillRect(pos.x - controller.getScaleDiv2(), pos.y - controller.getScaleDiv2(), controller.getScale(), controller.getScale());
		drawCanvasObject(g, "/Images/upper_node.png" , pos);
		paintGroupNodeBar(g, dGroupNode, pos);
	}
	private void paintGroupNodeBar(Graphics2D g, DecoratedGroupNode dGroupNode , Position pos) {
		// +1, -2, -1 little Adjustment for pixel perfect alignment
		int barWidth = (int) (controller.getScale());
		int barHeight = (int) (controller.getScale() / 5);
		g.setColor(Color.WHITE);
		g.fillRect(pos.x - controller.getScaleDiv2(), pos.y + controller.getScaleDiv2() - 1, (int) barWidth, barHeight);
		float[] percentages = getGroupNodeBarPercentages(dGroupNode);
		Color[] colors = new Color[6];
		colors[0] = getStateColor(HolonObjectState.PRODUCER);
		colors[1] = getStateColor(HolonObjectState.NOT_SUPPLIED);
		colors[2] = getStateColor(HolonObjectState.PARTIALLY_SUPPLIED);
		colors[3] = getStateColor(HolonObjectState.SUPPLIED);
		colors[4] = getStateColor(HolonObjectState.OVER_SUPPLIED);
		colors[5] = getStateColor(HolonObjectState.NO_ENERGY);
				
		for(int i = 5; i>=0; i--) {
			g.setColor(colors[i]);
			g.fillRect(pos.x - controller.getScaleDiv2(), pos.y + controller.getScaleDiv2() - 1, (int) (barWidth * percentages[i] - 1), barHeight);		
		}
//		g.setColor(color);
//		g.fillRect(pos.x - controller.getScaleDiv2(), pos.y + controller.getScaleDiv2() - 1, (int) (barWidth * (percentage < 1 ? percentage : 1.0f) - 1), barHeight);
		g.setColor(Color.BLACK);
		g.setStroke(new BasicStroke(1));
		g.drawRect(pos.x - controller.getScaleDiv2(), pos.y + controller.getScaleDiv2() - 1, barWidth - 1 , barHeight);
	}
	/**
	 * HardCoded Stuff dont try at Home ;)
	 * @param dGroupNode
	 * @return
	 */
	public float[] getGroupNodeBarPercentages(DecoratedGroupNode dGroupNode) {
		int[] amountOfObjects = new int[6];
		amountOfObjects[0] = dGroupNode.getAmountOfSupplier();
		amountOfObjects[1] = dGroupNode.getAmountOfConsumerWithState(HolonObjectState.NOT_SUPPLIED);
		amountOfObjects[2] = dGroupNode.getAmountOfConsumerWithState(HolonObjectState.PARTIALLY_SUPPLIED);
		amountOfObjects[3] = dGroupNode.getAmountOfConsumerWithState(HolonObjectState.SUPPLIED);
		amountOfObjects[4] = dGroupNode.getAmountOfConsumerWithState(HolonObjectState.OVER_SUPPLIED);
		amountOfObjects[5] = dGroupNode.getAmountOfPassiv();
		int countHolonObjects = amountOfObjects[0] + 	amountOfObjects[1] + amountOfObjects[2] + amountOfObjects[3] + amountOfObjects[4] + amountOfObjects[5];
		float[] percentages = new float[6];
		int count = 0;
		for(int i = 0; i < 6; i++) {
			count += amountOfObjects[i];
			percentages[i] = (float)count / (float)countHolonObjects;
		}
		return percentages;
	}
	private void paintSupplyBar(Graphics2D g, float percentage, Color color, Position pos) {
		// +1, -2, -1 little Adjustment for pixel perfect alignment
		int barWidth = (int) (controller.getScale());
		int barHeight = (int) (controller.getScale() / 5);
		g.setColor(Color.WHITE);
		g.fillRect(pos.x - controller.getScaleDiv2(), pos.y + controller.getScaleDiv2() - 1, (int) barWidth, barHeight);
		g.setColor(color);
		g.fillRect(pos.x - controller.getScaleDiv2(), pos.y + controller.getScaleDiv2() - 1, (int) (barWidth * (percentage < 1 ? percentage : 1.0f) - 1), barHeight);
		g.setColor(Color.BLACK);
		g.setStroke(new BasicStroke(1));
		g.drawRect(pos.x - controller.getScaleDiv2(), pos.y + controller.getScaleDiv2() - 1, barWidth - 1 , barHeight);
		g.setFont(new Font("TimesNewRoman", Font.PLAIN, (int) (barHeight * 1.5) - 2)); 
		String percentageString = (Math.round((percentage * 100))) + "%";
		int stringWidth = (int) g.getFontMetrics().getStringBounds(percentageString, g).getWidth();
		if(percentage > 1.0f) g.setColor(Color.WHITE); //Just to see better on purple
		g.drawString(percentageString, pos.x + 1 - stringWidth / 2, pos.y + controller.getScaleDiv2() - 1+ barHeight);
		
	}
	
	//old code
	void drawMarker(Graphics2D g) {
		Color transparentGrey = new Color(128, 174, 247, 40);
		if (sx > x && sy > y) {
			g.drawRect(x, y, sx - x, sy - y);
			g.setColor(transparentGrey);
			g.fillRect(x, y, sx - x, sy - y);
		} else if (sx < x && sy < y) {
			g.drawRect(sx, sy, x - sx, y - sy);
			g.setColor(transparentGrey);
			g.fillRect(sx, sy, x - sx, y - sy);
		} else if (sx >= x) {
			g.drawRect(x, sy, sx - x, y - sy);
			g.setColor(transparentGrey);
			g.fillRect(x, sy, sx - x, y - sy);
		} else if (sy >= y) {
			g.drawRect(sx, y, x - sx, sy - y);
			g.setColor(transparentGrey);
			g.fillRect(sx, y, x - sx, sy - y);
		}
	}
	public void paintComponent(Graphics g) {		
		super.paintComponent(g);
		Graphics2D g2d = (Graphics2D) g;
		g2d.setRenderingHints(new RenderingHints(RenderingHints.KEY_ANTIALIASING,
				RenderingHints.VALUE_ANTIALIAS_ON));
		//-->Old code
		if (drawEdge) {
			g2d.setColor(Color.BLACK);
			g2d.setStroke(new BasicStroke(1));
			g2d.drawLine(tempCps.getPosition().x, tempCps.getPosition().y, x, y);
		}
		//<--
		//SelectedCable
		HashSet<Edge> selectedEdges = new HashSet<Edge>();
		for(AbstractCanvasObject aCps:  model.getSelectedCpsObjects()) {
			for(Edge edge: aCps.getConnections()) {
				selectedEdges.add(edge);
			}
		}
		if(model.getSelectedEdge() != null) selectedEdges.add(model.getSelectedEdge());
		//timstep:
		g.setFont(new Font("TimesNewRoman", Font.PLAIN, Math.max((int) (controller.getScale() / 3.5f), 10) )); 
		g2d.setColor(Color.BLACK);
		
		VisualRepresentationalState  visualState = controller.getSimManager().getActualVisualRepresentationalState();
		//VisualState Representation:
		if(visualState == null) System.out.println("AHHH"); 
		for(ExitCable cable : visualState.getExitCableList()) {
			paintExitCable(g2d, cable);
		}
		for(DecoratedCable cable : visualState.getCableList()) {
			paintCable(g2d, cable, selectedEdges.contains(cable.getModel()));
		}
		for(DecoratedGroupNode dGroupNode : visualState.getGroupNodeList()) {
			paintGroupNode(g2d, dGroupNode);
		}
		for(Consumer con: visualState.getConsumerList()) {
			paintConsumer(g2d, con);					
		}
		for(Supplier sup: visualState.getSupplierList()) {
			paintSupplier(g2d, sup);				
		}
		for(Passiv pas: visualState.getPassivList()) {
			paintCanvasObject(g2d, pas);
		}
		for(DecoratedSwitch dSwitch : visualState.getSwitchList()) {
				paintSwitch(g2d, dSwitch);
		}
		for(Node node : visualState.getNodeList()) {
			drawCanvasObject(g2d, "/Images/node.png" , node.getPosition());
		}
		
		//-->oldCode 
		if (doMark) {
			g2d.setColor(Color.BLACK);
			g2d.setStroke(new BasicStroke(0));
			drawMarker(g2d);
		}
		//Test Selection
		//Objects:
		g2d.setColor(Color.BLUE);
		g2d.setStroke(new BasicStroke(1));
		Color transparentGrey = new Color(128, 174, 247, 40);
		for(AbstractCanvasObject aCps:  model.getSelectedCpsObjects()) {
			if(aCps instanceof Node) {
				Position pos = aCps.getPosition();
				g2d.setColor(transparentGrey);
				g2d.fillOval(pos.x - (int) (controller.getScaleDiv2()), pos.y - (int) (controller.getScaleDiv2()),  controller.getScale(),  controller.getScale());
				g2d.setColor(Color.LIGHT_GRAY);
				g2d.setStroke(new BasicStroke(2));
				g2d.drawOval(pos.x - (int) (controller.getScaleDiv2()), pos.y - (int) (controller.getScaleDiv2()),  controller.getScale(),  controller.getScale());
			}
			else {
				Position pos = aCps.getPosition();
				g2d.setColor(transparentGrey);
				g2d.fillRect(pos.x - (int) (controller.getScaleDiv2()* 1.5f), pos.y - (int) (controller.getScaleDiv2()* 1.5f), (int) (controller.getScale()* 1.5f) , (int) (controller.getScale()* 1.5f));
				g2d.setColor(Color.LIGHT_GRAY);
				g2d.setStroke(new BasicStroke(2));
				g2d.drawRect(pos.x - (int) (controller.getScaleDiv2()* 1.5f), pos.y - (int) (controller.getScaleDiv2()* 1.5f), (int) (controller.getScale()* 1.5f) , (int) (controller.getScale()* 1.5f));				
			}

		}
		//maybeReplace:
		if(mayBeReplaced != null){
			g2d.setColor(Color.RED);
			Position pos = mayBeReplaced.getPosition();
			g2d.drawImage(ImageImport.loadImage("/Images/replace.png") , 
					pos.x + controller.getScaleDiv2(),
					pos.y - controller.getScale(),
					controller.getScaleDiv2(), controller.getScaleDiv2(), null);
		}
		//<-- OldCode
	}

	@Override
	public void mouseClicked(MouseEvent e) {
		if(!disabled){
			if (e.getButton() == MouseEvent.BUTTON1) {
			DefaulTable propertyTable = model.getPropertyTable();
			if (propertyTable.getRowCount() > 0) {
				for (int i = propertyTable.getRowCount() - 1; i > -1; i--) {
					propertyTable.removeRow(i);
				}
			}

			triggerUpdateController();
			}
			stopEditing();
		}
	}

	@Override
	public void mouseEntered(MouseEvent e) {
	}

	@Override
	public void mouseExited(MouseEvent e) {
	}

	@Override
	public void mousePressed(MouseEvent e) {
		if(!disabled){
		stopEditing();
		tempCps = null;
		edgeHighlight = null;
		controller.setSelecteEdge(null);
		// Object Selection
		for (AbstractCanvasObject cps : model.getObjectsOnCanvas()) {
			cx = cps.getPosition().x - controller.getScaleDiv2();
			cy = cps.getPosition().y - controller.getScaleDiv2();
			if (x - controller.getScale() <= cx
					&& y - controller.getScale() <= cy && x >= cx && y >= cy) {
				tempCps = cps;


				dragging = true;

				if (e.isControlDown() && tempCps != null) {
					if (model.getSelectedCpsObjects().contains(tempCps)) {
						controller.deleteSelectedObject(tempCps);
							//TODO: RemoveDepth
					} else {
						controller.addSelectedObject(tempCps);
						if(tempCps instanceof GroupNode)
							controller.getObjectsInDepth();
					}

				}

				// If drawing an Edge (CTRL down)
				if (tempCps.getClass() == HolonObject.class) {
					HolonObject tempObj = ((HolonObject) tempCps);
					dataSelected = tempObj.getElements();
				}
				if (e.isShiftDown()) {
					drawEdge = true;
					dragging = false;
				}
			}
		}

		// Edge Selection
		if (tempCps == null) {
			edgeHighlight = mousePositionOnEdge(x, y);
			controller.setSelecteEdge(edgeHighlight);
			controller.setSelectedObjectID(0);
			if (!e.isControlDown() && e.getButton() != MouseEvent.BUTTON3) {
				model.getSelectedCpsObjects().clear();
			}
		}

		if (edgeHighlight == null && tempCps == null) {
			sx = e.getX();
			sy = e.getY();
			doMark = true;
		}

		repaint();
		}
	}

	@Override
	public void mouseReleased(MouseEvent e) {
		if(!disabled){
		x = e.getX();
		y = e.getY();

		dragging = false;

		if (drawEdge) {
			drawEdge = false;
			drawDeleteEdge();
		}

		if (dragged) {
			try {
				/**
				 * Save before further Dragged interactions happen
				 */
				controller.autoSave();
				
			} catch (IOException ex) {
				System.err.println("AutoSave error by dragging");
				ex.printStackTrace();
			}
			
			/**
			 * check if a unique tempCps could replace an Object on the canvas
			 */
			if(model.getSelectedCpsObjects().size()==1 
					&& checkForReplacement(model.getObjectsOnCanvas(), tempCps, tempCps.getPosition().x, tempCps.getPosition().y)){
				/**
            	 * if UpperNode would be replaced, close its tabs
            	 */
            	if(mayBeReplaced instanceof GroupNode)
            		closeUpperNodeTab(mayBeReplaced.getId());
				/**
				 * replace on canvas (will save)
				 */
				controller.replaceCanvasObject(mayBeReplaced, tempCps);
				mayBeReplaced=null;
			}
		}

		if (!e.isControlDown() && !dragged && tempCps != null
				&& MouseEvent.BUTTON3 != e.getButton()) {
			model.getSelectedCpsObjects().clear();
			controller.addSelectedObject(tempCps);
			model.setSelectedCpsObject(tempCps);
			if(tempCps instanceof GroupNode)
				controller.getObjectsInDepth();
		}

		dragged = false;

		// Rightclick List
		setRightClickMenu(e);

		markObjects();

		if (doubleClick() && tempCps instanceof HolonSwitch 
				&& MouseEvent.BUTTON3 != e.getButton()&& tempCps != null) {
			((HolonSwitch) tempCps).switchState();

		}
		controller.calculateStateAndVisualForTimeStep(model.getCurIteration());

		triggerUpdateController();

		repaint();
		}
	}

	@Override
	public void mouseDragged(MouseEvent e) {
		if(!disabled){
		// If Edge is drawn
		x = e.getX();
		y = e.getY();
		
		if (!model.getSelectedCpsObjects().contains(tempCps) && !doMark) {
			model.getSelectedCpsObjects().clear();
			if (tempCps != null) {
				controller.addSelectedObject(tempCps);
			}
		}
		if (dragging) {
			try {
				dragged = true;
				float xDist, yDist; // Distance

				x = e.getX();
				y = e.getY();

				// Make sure its in bounds
				int scaleDiv2 = controller.getScaleDiv2();
				if (e.getX() < scaleDiv2)
					x = scaleDiv2;
				else if (e.getX() > this.getWidth() - scaleDiv2)
					x = this.getWidth() - scaleDiv2;
				if (e.getY() < scaleDiv2)
					y = scaleDiv2;
				else if (e.getY() > this.getHeight()
						- scaleDiv2)
					y = this.getHeight() - scaleDiv2;

				// Distance
				xDist = x - tempCps.getPosition().x;
				yDist = y - tempCps.getPosition().y;

				tempCps.setPosition(x, y); // Drag Position
				// ToolTipText Position and name
				toolTip = true;
				toolTipText = tempCps.getName() + ", " + tempCps.getId();
				toolTipPos.x = tempCps.getPosition().x
						- scaleDiv2;
				toolTipPos.y = tempCps.getPosition().y
						+ scaleDiv2;

				// All Selected Objects
				for (AbstractCanvasObject cps : model.getSelectedCpsObjects()) {
					if (cps != tempCps) {
						x = (int) (cps.getPosition().x + xDist);
						y = (int) (cps.getPosition().y + yDist);

						// Make sure its in bounds
						if (x <= scaleDiv2)
							x = scaleDiv2;
						else if (x > this.getWidth()
								- scaleDiv2)
							x = this.getWidth() - scaleDiv2;
						if (y <= scaleDiv2)
							y = scaleDiv2;
						else if (y > this.getHeight()
								- scaleDiv2)
							y = this.getHeight() - scaleDiv2;

						cps.setPosition(x, y);
					}
				}
				
				/**
				 * check if something might be replaced
				 */
				if(model.getSelectedCpsObjects().size()==1)
					checkForReplacement(model.getObjectsOnCanvas(), tempCps, x, y);
				
				repaint();
			} catch (Exception eex) {
			}
		}

		// Mark Objects
		if (doMark) {
			tempSelected.clear();
			for (AbstractCanvasObject cps : model.getObjectsOnCanvas()) {
				int x1 = sx, x2 = x, y1 = sy, y2 = y;

				if (sx >= x) {
					x1 = x;
					x2 = sx;
				}
				if (sy >= y) {
					y1 = y;
					y2 = sy;
				}
				if (x1 <= cps.getPosition().x + model.getScaleDiv2()
						&& y1 <= cps.getPosition().y + model.getScaleDiv2()
						&& x2 >= cps.getPosition().x
						&& y2 >= cps.getPosition().y) {
					tempSelected.add(cps);

				}
			}
		}

		repaint();
		}
	}

	@Override
	public void mouseMoved(MouseEvent e) {	
		if(!disabled){
		x = e.getX();
		y = e.getY();

		// Everything for the tooltip :)
		boolean on = false;
		for (AbstractCanvasObject cps : model.getObjectsOnCanvas()) {
			cx = cps.getPosition().x - controller.getScaleDiv2();
			cy = cps.getPosition().y - controller.getScaleDiv2();

			on = setToolTipInfoAndPosition(on, cps);
		}
		if(on||(!on && toolTip))
		repaint();
		toolTip = on;
		}
	}


	/**
	 * Draws or Deletes an Edge.
	 */
	void drawDeleteEdge() {
		if (getMousePosition() != null) {
			boolean node = true;
			boolean newEdge = true;
			boolean onEdge = true;
			boolean deleteNode = false;
			Edge e = null;

			for (AbstractCanvasObject cps : model.getObjectsOnCanvas()) {
				cx = cps.getPosition().x - controller.getScaleDiv2();
				cy = cps.getPosition().y - controller.getScaleDiv2();
				if (x - controller.getScale() <= cx
						&& y - controller.getScale() <= cy && x >= cx
						&& y >= cy && cps != tempCps) {
					node = false;
					onEdge = false;
					for (Edge p : tempCps.getConnections()) {
						if ((p.getA() == tempCps && p.getB() == cps)
								|| (p.getB() == tempCps && p.getA() == cps)) {
							newEdge = false;
							e = p;
						}
					}
					if (!newEdge) {
						controller.removeEdgesOnCanvas(e);
						// Node ohne Edge?
						if (e.getA().getClass() == Node.class
								&& e.getA().getConnections().isEmpty()) {
							tempCps = e.getA();
							deleteNode = true;
						}
						if (e.getB().getClass() == Node.class
								&& e.getB().getConnections().isEmpty()) {
							deleteNode = true;
						}
					} else {
						if(!(cps instanceof GroupNode || tempCps instanceof GroupNode)) {
							e = new Edge(cps, tempCps, model.getMaxCapacity());
							controller.addEdgeOnCanvas(e);
						}else if (cps instanceof GroupNode && !(tempCps instanceof GroupNode)){
							GroupNode thisUpperNode = (GroupNode)cps;
							Object[] possibilities = thisUpperNode.getNodes().stream().map(aCps -> new ACpsHandle(aCps)).filter(aCpsHandle -> !(aCpsHandle.object instanceof GroupNode)).toArray();
							if(possibilities.length != 0) {
								ACpsHandle selected = (ACpsHandle) JOptionPane.showInputDialog(this, "Select a inside Object:", "Connection To?",  JOptionPane.OK_OPTION,new ImageIcon(new BufferedImage(1, 1, BufferedImage.TYPE_INT_ARGB)) , possibilities, "");
								if(selected != null) {
									e = new Edge(selected.object, tempCps, model.getMaxCapacity());
									controller.addEdgeOnCanvas(e);
								}
							}else {
								Node n = new Node("Node");

								n.setPosition(x, y);
								controller.addObjUpperNode(n, thisUpperNode);
								e = new Edge(n, tempCps, model.getMaxCapacity());
								controller.addEdgeOnCanvas(e);
							}
							
						}
						
					}
				}
			}
			// Edge auf eine Edge gezogen?
			if (onEdge && !checkForReplacement(x, y)) {
				Edge p = mousePositionOnEdge(x, y);
				if (p != null) {
					Edge e1;
					Edge e2;

					node = false;

					Node n = new Node("Node");

					n.setPosition(x, y);
					controller.addObjectCanvas(n);

					AbstractCanvasObject r, k;
					r = p.getA();
					k = p.getB();

					e = new Edge(n, tempCps, model.getMaxCapacity());

					e1 = new Edge(n, r, model.getMaxCapacity());

					e2 = new Edge(n, k, model.getMaxCapacity());

					controller.removeEdgesOnCanvas(p);
					controller.addEdgeOnCanvas(e);
					controller.addEdgeOnCanvas(e1);
					controller.addEdgeOnCanvas(e2);
				}
			}else{
				mayBeReplaced = null;
			}

			// ins leere Gedragged
			if (node && !checkForReplacement(x, y)) {
				Node n = new Node("Node");

				n.setPosition(x, y);
				
					controller.addObjectCanvas(n);

				e = new Edge(n, tempCps, model.getMaxCapacity());

				controller.addEdgeOnCanvas(e);
			}else{
				mayBeReplaced = null;
			}

			// Wenn ein Node ohne Connections da ist
			if (deleteNode) {
				controller.delCanvasObject(tempCps, true);
				tempCps = null;
			}
		}
	}

	/**
	 * Checks if the mouse is on an Edge.
	 *
	 * @param x
	 *            Position of the Mouse
	 * @param y
	 *            Position of the Mouse
	 * @return CpsEdge the Mouse is on, null if the mouse is not on an Edge
	 */
	private Edge mousePositionOnEdge(int x, int y) {
		x += controller.getScaleDiv2();
		y += controller.getScaleDiv2();
		for (Edge p : model.getEdgesOnCanvas()) {
			Line2D l = new Line2D.Float(p.getA().getPosition().x, p.getA()
					.getPosition().y, p.getB().getPosition().x, p.getB()
					.getPosition().y);

			int[] positions = determineMousePositionOnEdge(p);
			int lx = positions[0];
			int ly = positions[1];
			int hx = positions[2];
			int hy = positions[3];

			// distance from a point to a line and between both Objects
			if (l.ptLineDistSq(x - model.getScaleDiv2(),
					y - model.getScaleDiv2()) < 20
					&& x > lx && x < hx && y > ly && y < hy) {
				return p;
			}
		}
		return null;
	}

	void updateLanguages() {
		itemCut.setText("Cut");
		itemCopy.setText("Copy");
		itemPaste.setText("Paste");
		itemDelete.setText("Delete");
		itemGroup.setText("Group");
		itemUngroup.setText("Ungroup");
	}

	/**
	 * Set if Information should be shown.
	 *
	 * @param connection
	 *            boolean for conecction
	 * @param object
	 *            boolean for objects
	 * @param nodeOfnode
	 */
	void setShowedInformation(boolean connection, boolean object,
			boolean border, boolean nodeOfnode) {
		showConnectionInformation = connection;
		showedInformation[1] = object;
		showedInformation[3] = border;
		showedInformation[4] = nodeOfnode;
	}

	/**
	 * Returns if Information should be shown.
	 *
	 * @return Array of boolean [0] = connection, [1] = objects
	 */
	boolean[] getShowedInformation() {
		return showedInformation;
	}

	/**
	 * set toolTip
	 *
	 * @param bool
	 */
	void setToolTip(boolean bool) {
		this.toolTip = bool;
	}

	/**
	 * Set the Mouse
	 *
	 * @param x
	 * @param y
	 */
	void setXY(int x, int y) {
		this.x = x;
		this.y = y;
	}

	@Override
	public boolean checkForReplacement(int x, int y) {
		
		return checkForReplacement(model.getObjectsOnCanvas(), null, x, y);
	}
	
	@Override
	public void tryToAlignObjects(){
		/**
		 * Align all Objects
		 */
		for(AbstractCanvasObject cps: model.getObjectsOnCanvas())
			align(cps,3*model.getScaleDiv2());
		
		/**
		 * AutoSave new Positons
		 */
		try{	
			controller.autoSave();
		} catch (IOException ex) {
			System.err.println("AutoSave error by aligning");
			ex.printStackTrace();
		}
	}
	
	
	@Override
	public void closeUpperNodeTab(int upperNodeId) {
		JTabbedPane tabbedPaneInner = (JTabbedPane) getParent()
				.getParent().getParent().getParent();
		for (int i = 1; i < tabbedPaneInner.getTabCount(); i++) {
			if (((GroupNodeCanvas) ((JScrollPane) tabbedPaneInner
					.getComponentAt(i)).getViewport().getComponent(
							0)).upperNode.getId() == upperNodeId) {
				tabbedPaneInner.remove(i);
				break;
			}
		}
	}
	
	
	   
}