package ui.view;

import javax.swing.table.DefaultTableModel;

/**
 * Property Table.
 * 
 * @author Gruppe14
 */
public class PropertyTable extends DefaultTableModel {

	private final int maxColumns = 7;
	
	@Override
	public Class<?> getColumnClass(int columnIndex) {
		//System.out.println("getColumnClass(" + "index:" + columnIndex+ ", ColumnCount:" + getColumnCount() + ")");
		if (getColumnCount() == maxColumns) {
			switch (columnIndex) {
			case 4:
			case 6:
				return Boolean.class;
			}
		} else if (getColumnCount() == maxColumns - 1) {
			switch (columnIndex) {
			case 3:
			case 5:
				return Boolean.class;
			}
		}
		return String.class;
	}
	
	@Override
	public boolean isCellEditable(int row, int column) {
		return getColumnCount() == maxColumns && column > 1
				|| getColumnCount() == maxColumns -1  && column > 0;
	}
}
