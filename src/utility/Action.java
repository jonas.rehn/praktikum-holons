package utility;

import java.util.HashSet;
import java.util.Set;
import java.util.function.Consumer;

public class Action<T> {
    private Set<Consumer<T>> listeners = new HashSet<Consumer<T>>();

    public void addListener(Consumer<T> listener) {
        listeners.add(listener);
    }

    public void broadcast(T argument) {
        listeners.forEach(x -> x.accept(argument));
    }
}
