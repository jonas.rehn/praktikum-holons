package utility;
import java.util.HashSet;
import java.util.Set;

public class Event {
    private Set<Runnable> listeners = new HashSet<Runnable>();
    public void addListener(Runnable listener) {
        listeners.add(listener);
    }

    public void broadcast() {
        listeners.forEach(x -> x.run());
    }
}
