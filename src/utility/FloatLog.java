package utility;

import java.util.HashMap;
import java.util.stream.Collectors;
//To sample floats and get basic statistical values;
public class FloatLog {
	public class LogInfo{
		public String parameterName;
		public int sampleSize = 0;
		public float min = Float.MAX_VALUE;
		public float max = Float.MIN_VALUE;
		private float sum = 0;
		public float getAvg() {
			if(sampleSize > 0)
				return sum / (float)sampleSize;
			return 0;
		}
		public void addSample(float sample) {
			if(sample < min) min = sample;
			if(sample > max) max = sample;
			sum += sample;
			sampleSize++;
		}
		public String toString() {
			return "[" + parameterName + " in Range(" + min + ", " + max + ") with a Average of " + getAvg() + " from "+ sampleSize + " Smaples]"; 
		}
	}
	public HashMap<String, LogInfo> logMap = new HashMap<String, LogInfo>();
	public void addSample(String parameterName, float sample) {
		//Puts the sample in the right LogInfo container or create a new container if not exist jet
		if(logMap.containsKey(parameterName)) {
			LogInfo info = logMap.get(parameterName);
			info.addSample(sample);
		}else {
			LogInfo info = new LogInfo();
			info.parameterName = parameterName;
			info.addSample(sample);
			logMap.put(parameterName, info);
		}
	}
	public String toString() {
		return logMap.values().stream().map(Object::toString).collect(Collectors.joining("\n"));
	}
	public void clear() {
		logMap.clear();
	}
}
