package utility;





public class Random {
		
		
		private static java.util.Random random = new java.util.Random();
	
		/**
		 * True or false
		 * @return the random boolean.
		 */
		public static boolean nextBoolean(){
			return random.nextBoolean();
		}
		/**
		 * Between 0.0(inclusive) and 1.0 (exclusive)
		 * @return the random double.
		 */
		public static double nextDouble() {
			return random.nextDouble();
		}
		
		public static float nextFloatInRange(float min, float max) {
			return min + random.nextFloat() * Math.abs(max - min);
		}
		/**
		 * Returns the next pseudorandom, Gaussian ("normally") distributed double value with mean
		 * and standard deviation from this random number generator's sequence. 
		 * @param mean
		 * @param deviation
		 * @return
		 */
		public static double nextGaussian(double mean, double deviation) {
			return mean + random.nextGaussian() * deviation;
		}
		public static double nextDoubleInRange(double min, double max) {
			return min + random.nextDouble() * Math.abs(max - min);
		}
		
		/**
		 * Random Int in Range [min;max[ with UniformDistirbution
		 * @param min
		 * @param max
		 * @return
		 */
		public static int nextIntegerInRange(int min, int max) {
			return min + random.nextInt(max - min);
		}
		/**
		 * Random Int in Range [min;max[ with UniformDistirbution
		 * @param min
		 * @param max
		 * @param valueBetween a value between min and max
		 * @return
		 */
		public static int nextIntegerInRangeExcept(int min, int max,int valueBetween) {
			int result = min;
			if(max - min == 1) {
				//return the other value
				return (valueBetween == min)? max:min;
			}
			try {
				result = min + random.nextInt((max - 1) - min);
				if(result >= valueBetween) {
					result++;
				}
			}catch(java.lang.IllegalArgumentException e){
				System.err.println("min : " + min + " max : " + max + " valueBetween:" + valueBetween);
				System.err.println("Except max should be more then min");
			}
			return result;
		}
}
