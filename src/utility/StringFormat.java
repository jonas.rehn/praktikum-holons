package utility;

import java.math.RoundingMode;
import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.util.Locale;

public class StringFormat {
	
	private static DecimalFormat formatter;
	private static DecimalFormat twoFormatter;
	
	static {
		NumberFormat nf = NumberFormat.getNumberInstance(Locale.US);
		formatter = (DecimalFormat) nf;
		formatter.applyPattern("#.###########");
		formatter.setRoundingMode(RoundingMode.UP);
		
		twoFormatter = (DecimalFormat) nf;
		twoFormatter.applyPattern("#.##");
		twoFormatter.setRoundingMode(RoundingMode.UP);
	}
	public static String doubleFixedPlaces(int places, double value) {
		return String.format(Locale.US, "%." +  places  + "f", value);
	}
	
	public static String doubleTwoPlaces(double value) {
		return twoFormatter.format(value);
	}
	
	
	public static String doubleAllPlaces(double value) {
		return formatter.format(value);
	}
}
