package tests;

import java.util.ArrayList;
import static java.lang.Math.*;

import classes.Category;
import classes.AbstractCanvasObject;
import classes.HolonElement;
import classes.HolonObject;
import classes.IdCounterElem;

/**
 * This Class Generates Stuff for the Tests.
 * 
 * @author Gruppe14
 */
public class PraktikumHolonsAdapter {

	
	/**
	 * Generate Sequence of Categories from A - ZZZ.
	 * @param arr ArrayList of Categories
	 */
	public void generateCategories(ArrayList<Category> arr) {
		for (int i = 1; i < 18279; i++) {
			arr.add(new Category(generate(i)));
		}
	}
	
	/**
	 * Generate Sequence of Objects from A - ZZZ.
	 * @param arr ArrayList of Categories
	 */
	public void generateObjects(ArrayList<AbstractCanvasObject> arr) {
		for (int i = 1; i < 18279; i++) {
			arr.add(new HolonObject(generate(i)));
		}
	}
	
	/**
	 * Generate Sequence of Elements from A - ZZZ.
	 * @param arr ArrayList of Categories
	 */
	public void generateElements(ArrayList<HolonElement> arr) {
		for (int i = 1; i < 18279; i++) {
			arr.add(new HolonElement(null, generate(i), 1, 10, IdCounterElem.nextId()));
		}
	}
	
	/**
	 * Generate Alphabetical Sequences.
	 * @param n Generator
	 * @return A generated Alphabetical Sequence
	 */
	public String generate(int n) {
	    char[] buf = new char[(int) floor(log(25 * (n + 1)) / log(26))];
	    for (int i = buf.length - 1; i >= 0; i--) {
	        n--;
	        buf[i] = (char) ('A' + n % 26);
	        n /= 26;
	    }
	    return new String(buf);
	}
	
}
