package tests;

import org.junit.Before;
import org.junit.Test;
import static org.junit.Assert.*;

import ui.controller.AutoSaveController;
import ui.model.Model;
/**
 * Tests for the AutoSaveController.
 * 
 * @author Gruppe14
 */
public class PraktikumHolonsTestAutoSaveController {
	protected Model model;
	protected AutoSaveController controller;


	/**
	 * Setup for the test.
	 */
	@Before
	public void setUp() {
		model = new Model();
		controller = new AutoSaveController(model);
	}

	/**
	 *  Tests for the AutoSavController. 
	 *  The AutoSaveController only contains indices for the actual save file.
	 */
	@Test
	public void testAutoSave() {
		int temp = controller.getAutoSaveNr();
		assertTrue("AutoSave Number not forrect", temp == -1);
		controller.increaseAutoSaveNr();
		assertTrue("AutoSave Number not forrect", controller.getAutoSaveNr() == 0);
		
		for (int i = 0; i < 20; i++) {
			controller.increaseAutoSaveNr();
		}
		assertTrue("AutoSave Number not correct", controller.getAutoSaveNr() == 20);
		assertTrue("Is Allowed not set", controller.allowed());
		
		for (int i = 0; i < 20; i++) {
			controller.decreaseAutoSaveNr();;
		}
		assertTrue("AutoSave Number not correct", controller.getAutoSaveNr() == 0);
		
	}
}
