package tests;

import static org.junit.Assert.assertTrue;

import org.junit.Before;
import org.junit.Test;

import ui.controller.GlobalController;
import ui.model.Model;

/**
 * Test for the GlobalController.
 * 
 * @author Gruppe14
 */
public class PraktikumHolonsTestGlobalController {

	protected Model model;
	protected GlobalController controller;

	/**
	 * Setup.
	 */
	@Before
	public void setUp() {
		model = new Model();
		controller = new GlobalController(model);

	}

	/**
	 * Test for GlobalControls.
	 */
	@Test
	public void testGlobalControls() {
		int prevScale = controller.getScale();
		int prevScaleDiv2 = controller.getScaleDiv2();
		int prevNumberSav = controller.getNumbersOfSaves();
		int timer = model.getTimerSpeed();
		int it = model.getCurIteration();

		controller.setScale(100);
		controller.setNumberOfSaves(50);
		controller.setTimerSpeed(2000);
		controller.setCurIteration(10);

		assertTrue("Scale was not changed", controller.getScale() != prevScale);
		assertTrue("ScaleDiv2 was not changed ", model.getScaleDiv2() != prevScaleDiv2);
		assertTrue("Number of Saves was not changed", controller.getNumbersOfSaves() != prevNumberSav);
		assertTrue("Timer speed was not changed", timer != model.getTimerSpeed());
		assertTrue("Curr Iteration was not Set", it != model.getCurIteration());

	}

}
