package tests;

import junit.framework.JUnit4TestAdapter;
import junit.framework.Test;
import junit.framework.TestSuite;

/**
 * Test Suite for the Tests.
 * 
 * @author Gruppe14
 */
public class PraktikumHolonsTestSuite {


	/**
	 * Test Suite. 
	 * 
	 * @return suite
	 */
	public static Test suite() {
		TestSuite suite = new TestSuite("Tests for prakikum-holons");
		suite.addTest(new JUnit4TestAdapter(PraktikumHolonsTestCategoryController.class));
		suite.addTest(new JUnit4TestAdapter(PraktikumHolonsTestCanvasController.class));
		suite.addTest(new JUnit4TestAdapter(PraktikumHolonsTestObjectController.class));
		suite.addTest(new JUnit4TestAdapter(PraktikumHolonsTestLoadAndStoreController.class));
		suite.addTest(new JUnit4TestAdapter(PraktikumHolonsTestGlobalController.class));
		suite.addTest(new JUnit4TestAdapter(PraktikumHolonsTestAutoSaveController.class));
		suite.addTest(new JUnit4TestAdapter(PraktikumHolonsTestClasses.class));
		return suite;
	}
}
